//
//  PartnerDetailViewController.swift
//  Ured
//
//  Created by Isaias Carrera Ventura on 01/12/17.
//  Copyright © 2017 IQBC. All rights reserved.
//

import UIKit
import MapKit
import Kingfisher
import Cosmos
import Alamofire

class PartnerDetailViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, MKMapViewDelegate {
    
    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var namePartner: UILabel!
    @IBOutlet weak var categoryPartner: UILabel!
    @IBOutlet weak var rankingPartner: CosmosView!
    @IBOutlet weak var imageView: UIImageView!
    
    var reviewList = [ReviewPartner]()
    var partnerModel: PartnerModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        CustomBar.setNavBar((self.navigationController?.navigationBar)!, navItem: navigationItem,nav: self)
        setupView()
        
    }
    
    
    
    func setupView(){
        
        
        self.namePartner.text = (partnerModel.value(forKey: "name") as! String) + " " + (partnerModel.value(forKey: "lastname") as! String)
        self.categoryPartner.text = partnerModel.value(forKey: "category") as? String
        self.rankingPartner.rating = Double(partnerModel.value(forKey: "ranking") as! Int)
        
        if let url = URL(string: partnerModel.value(forKey: "imageUrl") as! String){
            
            KingfisherManager.shared.retrieveImage(with: url, options: [.transition(.fade(1))], progressBlock: nil, completionHandler: { image, error, cacheType, imageURL in
                
                if error == nil {
                    
                    self.imageView.image = image
                    
                }
                
            })
            
        }
        
        let location = CLLocationCoordinate2D(latitude: partnerModel.latitude, longitude: partnerModel.longitude)
        ImageUtils.circularImage(imageView, color: ColorUtils.getColorUredApp(1.0).cgColor)
        addRadiusCircle(location: location)
        
        
        let latDelta:CLLocationDegrees = 0.03
        let lonDelta:CLLocationDegrees = 0.03
        let span = MKCoordinateSpanMake(latDelta, lonDelta)
        let locationReggion = CLLocationCoordinate2DMake(location.latitude, location.longitude)
        let region = MKCoordinateRegionMake(locationReggion, span)
        
        mapView.setRegion(region, animated: false)
        
        getReviewForPartner()
        
    }
    
    func getReviewForPartner() {
        
        let headers : HTTPHeaders = ["Authorization" : "JWT \(UserController.getUserSession()!.value(forKey: "jwt") as! String)"]
        
        Alamofire.request(Constants.REVIEW_ENDPOINT + "\((partnerModel.value(forKey: "partnerId") as! String))", method: .get, headers: headers)
        .validate()
        .response { (dataResponse) in
            
            if let partnerComment = ReviewController.getReviewList(dataResponse){
                
                
                if partnerComment.count > 0{
                    
                    self.reviewList.removeAll()
                    self.reviewList.append(contentsOf: partnerComment)
                    self.tableView.reloadData()
                    
                }
                
                
            }
        }
        
    }
    
    func addRadiusCircle(location: CLLocationCoordinate2D){
        self.mapView.delegate = self
        let circle = MKCircle(center: location, radius: 500)
        self.mapView.add(circle)
    }
    
    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        let circle = MKCircleRenderer(overlay: overlay)
        circle.strokeColor = UIColor.red
        circle.fillColor = UIColor(red: 255, green: 0, blue: 0, alpha: 0.1)
        circle.lineWidth = 1
        return circle
        
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func requestService(_ sender: Any) {
        
        self.performSegue(withIdentifier: "requestServiceSegue", sender: self)
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "requestServiceSegue" {
            
            let vc = segue.destination as! PartnerRequestServiceViewController
            vc.partnerModel = self.partnerModel
            
        }
        
    }
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return reviewList.count
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        tableView.estimatedRowHeight = 100
        tableView.rowHeight = UITableViewAutomaticDimension
        
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        
        //CellBook
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! ReviewTableViewCell
        
        cell.rankingPartner.rating = Double(reviewList[indexPath.row].ranking)
        cell.commentText.text = reviewList[indexPath.row].comment
        
        return cell
        
    }
}
