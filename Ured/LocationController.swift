//
//  LocationController.swift
//  Ured
//
//  Created by Isaias Carrera Ventura on 06/12/17.
//  Copyright © 2017 IQBC. All rights reserved.
//

import Foundation
import RealmSwift
import SwiftyJSON
import Alamofire

class LocationController{
    
    class func saveLocation(_ location: LocationUser) {
        
        let realm = try! Realm()
        try! realm.write {
            
            realm.add(location)
            
        }
        
    }
    
    class func removeLocation(_ location: LocationUser){
        
        let realm = try! Realm()
        let predicate = NSPredicate(format: "idLocation = %@", (location.value(forKey: "idLocation") as! String))
        let locToDelete = realm.objects(LocationUser.self).filter(predicate)
        
        try! realm.write {
            
            realm.delete(locToDelete)
            
        }
        
    }
    
    class func getLocationInDisk() -> [LocationUser]{
        
        let realm = try! Realm()
        let locations = realm.objects(LocationUser.self)
        return Array(locations)
        
    }
    
    class func saveListLocation(_ locationList : [LocationUser]) {
        
        for location in locationList{
            
            saveLocation(location)
            
        }
    }
    
    class func saveRepeatedLocationList(_ locationList: [LocationUser]) {
        
        let listInDB = getLocationInDisk()
        
        for location in locationList{
            
            var isInDisk = false
            
            for locationDB in listInDB{
                
                if location.value(forKey: "idLocation") as! String == locationDB.value(forKey: "idLocation") as! String {
                    
                    isInDisk = true
                    
                }
                
            }
            
            if !isInDisk{
                
                saveLocation(location)
                
            }
            
        }
        
    }
    
    class func getListFromResponse(_ dataResponse: DefaultDataResponse) -> [LocationUser]?{
        
        guard dataResponse.error == nil else{
            return nil
        }
        
        guard let data = dataResponse.data else{
            return nil
        }
        
        let json = JSON(data:data)
        let jsonAddress = json["addresses"]
        var locationList = [LocationUser]()
        
        for (_,subJson):(String,JSON) in jsonAddress{
            
            let name = subJson["name"].stringValue
            let address = subJson["address"].stringValue
            let latitude = subJson["latitude"].doubleValue
            let longitude = subJson["longitude"].doubleValue
            let id = subJson["id"].stringValue
            
            let location = LocationUser()
            location.name = name
            location.address = address
            location.latitude = latitude
            location.longitude = longitude
            location.idLocation = id
            
            locationList.append(location)
            
        }
        
        return locationList.count > 0 ? locationList : nil
        
    }
    
    class func getStatusResponseForDeletion(_ dataResponse: DefaultDataResponse) -> Bool {
        guard dataResponse.error == nil else{
            return false
        }
        
        guard let data = dataResponse.data else{
            return false
        }
        
        let json = JSON(data:data)
        print(json)
        return true
        
    }
    
    
    class func getAddressFromResponse(_ dataResponse: DefaultDataResponse) -> LocationUser?{
        
        guard dataResponse.error == nil else{
            return nil
        }
        
        guard let data = dataResponse.data else{
            return nil
        }
        
        let json = JSON(data:data)
        
        let name = json["name"].stringValue
        let address = json["address"].stringValue
        let latitude = json["latitude"].doubleValue
        let longitude = json["longitude"].doubleValue
        let id = json["id"].stringValue
        
        let location = LocationUser()
        location.name = name
        location.address = address
        location.latitude = latitude
        location.longitude = longitude
        location.idLocation = id
        
        return location
    }
    
    
    
}
