//
//  AlertPartnerViewController.swift
//  Ured
//
//  Created by Isaias Carrera Ventura on 29/11/17.
//  Copyright © 2017 IQBC. All rights reserved.
//

import UIKit
import CarbonKit

class AlertPartnerViewController: UIViewController, CarbonTabSwipeNavigationDelegate {
    
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        let items = ["Urgentes", "Últimos"]
        let carbonTabSwipeNavigation = CarbonTabSwipeNavigation(items: items, delegate: self)
        
        carbonTabSwipeNavigation.setNormalColor(ColorUtils.getGrayLightColorApp(1.0))
        carbonTabSwipeNavigation.setSelectedColor(ColorUtils.getColorUredApp(1.0))
        carbonTabSwipeNavigation.setIndicatorColor(ColorUtils.getColorUredApp(1.0))
        carbonTabSwipeNavigation.carbonSegmentedControl?.backgroundColor = ColorUtils.getGrayColorApp(1.0)
        carbonTabSwipeNavigation.carbonTabSwipeScrollView.backgroundColor = ColorUtils.getGrayColorApp(1.0)
        
        let width = self.view.frame.width / 2
        
        carbonTabSwipeNavigation.carbonSegmentedControl?.setWidth(width, forSegmentAt: 0)
        carbonTabSwipeNavigation.carbonSegmentedControl?.setWidth(width, forSegmentAt: 1)
        
        carbonTabSwipeNavigation.insert(intoRootViewController: self)
        
        
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        super.viewDidAppear(animated)

        CustomBar.setNavBarProfile((self.navigationController?.navigationBar)!, navItem: navigationItem,nav: self)
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func carbonTabSwipeNavigation(_ carbonTabSwipeNavigation: CarbonTabSwipeNavigation, viewControllerAt index: UInt) -> UIViewController {
        
        switch index {
        case 0:
            
            let viewControllerPublic = self.storyboard?.instantiateViewController(withIdentifier: "servicePartnerListViewController") as? PartnerServiceListViewController
            viewControllerPublic?.statusUrgent = true
            return viewControllerPublic!
            
        case 1:
            
            let viewControllerPrivate = self.storyboard?.instantiateViewController(withIdentifier: "servicePartnerListViewController") as? PartnerServiceListViewController
            viewControllerPrivate?.statusUrgent = false
            return viewControllerPrivate!
                        
        default:
            let viewControllerCooperationDetail = self.storyboard?.instantiateViewController(withIdentifier: "servicePartnerListViewController") as? PartnerServiceListViewController
            return viewControllerCooperationDetail!
            
        }
        
        
    }
    
 

}
