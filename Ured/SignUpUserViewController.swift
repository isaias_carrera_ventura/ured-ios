//
//  SignUpUserViewController.swift
//  Ured
//
//  Created by Isaias Carrera Ventura on 29/11/17.
//  Copyright © 2017 IQBC. All rights reserved.
//

import UIKit
import Alamofire
import NVActivityIndicatorView
import M13Checkbox
import SwiftValidator

class SignUpUserViewController: UIViewController , UITextFieldDelegate, ValidationDelegate, NVActivityIndicatorViewable{
    
    @IBOutlet weak var textFieldName: UITextField!
    @IBOutlet weak var textFieldLastname: UITextField!
    @IBOutlet weak var textFieldEmail: UITextField!
    @IBOutlet weak var textFieldPassword: UITextField!
    @IBOutlet weak var checkBox: M13Checkbox!
    
    @IBOutlet weak var navigationBar: UINavigationBar!
    @IBOutlet weak var customNavigationItem: UINavigationItem!
    
    let validator = Validator()
    
    @IBOutlet weak var scrollView: UIScrollView!
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        CustomBar.setNavBar((self.navigationBar)!, navItem: self.navigationItem,nav: self)
        
        validator.registerField(textFieldName, rules: [RequiredRule()])
        validator.registerField(textFieldLastname, rules: [RequiredRule()])
        validator.registerField(textFieldEmail, rules: [EmailRule()])
        validator.registerField(textFieldPassword, rules: [RequiredRule(message: "Contraseña requerida"),MinLengthRule(length: 6, message:" Mínimo 6 caractéres")])
        
        textFieldName.roundBorder()
        textFieldLastname.roundBorder()
        textFieldEmail.roundBorder()
        textFieldPassword.roundBorder()
        
    }
    
    override func didReceiveMemoryWarning() {
        
        super.didReceiveMemoryWarning()
        
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        if textField == textFieldEmail || textField == textFieldPassword{
            
            scrollView.setContentOffset(CGPoint(x:0,y:150), animated: true)
        }
        
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        scrollView.setContentOffset(CGPoint(x:0,y:0), animated: true)
        
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        textField.resignFirstResponder()
        return true
        
    }
    
    
    @IBAction func cancelAction(_ sender: Any) {
    
        self.dismiss(animated: true, completion: nil)
        
    }
    
    func validationSuccessful() {
    
        if checkBox.checkState == .checked{
            
            self.startAnimating(Constants.getSizeForLoadingView(view: self.view), type: NVActivityIndicatorType.ballBeat, color: ColorUtils.getColorUredApp(1.0), padding: NVActivityIndicatorView.DEFAULT_PADDING)
            let password = textFieldPassword.text!.sha1()
            let params : Parameters = ["name":textFieldName.text!,
                                       "lastname": textFieldLastname.text!,
                                       "email": textFieldEmail.text!,
                                       "password": password]
            
            Alamofire.request(Constants.SIGNUP_ENDPOINT, method: .post, parameters: params)
                .validate()
                .response { (dataResponse) in
                    
                    self.stopAnimating()
                    
                    if let user = UserController.getUserSignUpResponse(dataResponse){
                        
                        self.getUserInformation(user)
                        
                    }else{
                        
                        //TODO: Handle error
                        
                        _ = SweetAlert().showAlert("¡Algo salió mal!", subTitle: "¡Verifica tus datos y checa tu conexión a internet!", style: AlertStyle.error, buttonTitle:"Ok", buttonColor:ColorUtils.getGreenColorApp(1.0))
                        
                    }
                    
            }
            
        }else{
            
            
            _ = SweetAlert().showAlert("Ops", subTitle: "Acepta los términos y condiciones.", style: AlertStyle.error, buttonTitle:"Ok", buttonColor:ColorUtils.getGreenColorApp(1.0))
            
            self.view.endEditing(true)
            
        }
        
    }
    
    func getUserInformation(_ user: UserModel){
        
        self.startAnimating(Constants.getSizeForLoadingView(view: self.view), type: NVActivityIndicatorType.ballBeat, color: ColorUtils.getColorUredApp(1.0), padding: NVActivityIndicatorView.DEFAULT_PADDING)
        
        let headers : HTTPHeaders = ["Authorization" : "JWT \(user.value(forKey: "jwt") as! String)"]
        let url = Constants.USER_ENDPOINT + (user.value(forKey: "userId") as! String)
        
        Alamofire.request(url, method: .get,  headers: headers)
            .validate()
            .response { (dataResponse) in
                
                self.stopAnimating()
                
                if let userSave = UserController.getUserFromResponse(dataResponse){
                    
                    //TODO: Save user session
                    userSave.account = user.value(forKey: "account") as? String
                    userSave.jwt = user.value(forKey: "jwt") as? String
                    userSave.userId = user.value(forKey: "userId") as? String
                    
                    UserController.saveUserSession(userSave)
                    UserController.getUserLocationList(userSave)
                    
                    RedManager.setSessionType(Constants.userType)
                    self.performSegue(withIdentifier: "mainUserSegue", sender: self)
                    
                }else{
                    
                    //TODO: Handle error
                    
                    _ = SweetAlert().showAlert("¡Algo salió mal!", subTitle: "¡Verifica tus datos y checa tu conexión a internet!", style: AlertStyle.error, buttonTitle:"Ok", buttonColor:ColorUtils.getGreenColorApp(1.0))
                    
                }
                
        }
        
    }

    
    @IBAction func signUp(_ sender: Any) {
        
        validator.validate(self)
        
    }
    
    //mainUserSegue
    
    @IBAction func goToTerms(_ sender: Any) {
    
        guard let url = URL(string: Constants.TERMS_ENDPOINT) else {
            return //be safe
        }
        
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        } else {
            UIApplication.shared.openURL(url)
        }
        

        
    }
    
    func validationFailed(_ errors:[(Validatable ,ValidationError)]) {
        
        var errorString = ""
        
        // turn the fields to red
        for (field, error) in errors {
            if let field = field as? UITextField {
                field.layer.borderColor = UIColor.red.cgColor
                field.layer.borderWidth = 1.0
            }
            errorString = errorString + "\n\(error.errorMessage)"
            error.errorLabel?.text = error.errorMessage // works if you added labels
            error.errorLabel?.isHidden = false
        }
        
        self.view.endEditing(true)
        
        _ = SweetAlert().showAlert("Error", subTitle: "¡Ingresa los datos requeridos!\n\(errorString)", style: AlertStyle.error, buttonTitle:"Ok", buttonColor:ColorUtils.getGreenColorApp(1.0))
        
        
        
    }
    
}
