//
//  RedManager.swift
//  Ured
//
//  Created by Isaias Carrera Ventura on 06/12/17.
//  Copyright © 2017 IQBC. All rights reserved.
//

import Foundation
import UIKit

class RedManager {
    
    
    class func isTutorialShown() -> Bool {
        
        let userDefaults = UserDefaults.standard
        return userDefaults.bool(forKey: "walkthroughPresented")
        
    }
    
    class func removeStoredValues() -> Void {
        
        let userDefaults = UserDefaults.standard
        userDefaults.removeObject(forKey: "sessionType")
        userDefaults.removeObject(forKey: "session")
        userDefaults.synchronize()
        
    }
    
    class func setTutorialShown() {
        
        let userDefaults = UserDefaults.standard
        
        userDefaults.set(true, forKey: "walkthroughPresented")
        userDefaults.synchronize()
        
    }
    
    class func getSessionType() -> Int {
        
        let userDefaults = UserDefaults.standard
        return userDefaults.integer(forKey: "sessionType")
        
    }
    
    class func isSessionInit() -> Bool{
        
        let userDefaults = UserDefaults.standard
        return userDefaults.bool(forKey: "session")

    }
    
    class func setSessionType(_ type: Int){
        
        let userDefaults = UserDefaults.standard
        
        userDefaults.set(type, forKey: "sessionType")
        userDefaults.set(true, forKey: "session")
        userDefaults.synchronize()
        
    }
    
    
}

extension UITextField {
    
    func roundBorder(){
        
        self.layer.borderColor = ColorUtils.getColorUredApp(1.0).cgColor
        self.layer.borderWidth = 1
        self.layer.masksToBounds = true
        self.layer.cornerRadius = 5
        
    }
    
}


extension UITextView {
    
    func roundBorder(){
        
        self.layer.borderColor = ColorUtils.getColorUredApp(1.0).cgColor
        self.layer.borderWidth = 1.5
        self.layer.masksToBounds = true
        self.layer.cornerRadius = 10
        
    }
    
}
