//
//  ProtocolCategory.swift
//  Ured
//
//  Created by Isaias Carrera Ventura on 06/12/17.
//  Copyright © 2017 IQBC. All rights reserved.
//

import Foundation

protocol getCategoryBackDelegate{
    
    func writeCategoryBack(value: Category)
    
}
