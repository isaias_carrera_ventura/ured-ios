//
//  Category.swift
//  Ured
//
//  Created by Isaias Carrera Ventura on 30/11/17.
//  Copyright © 2017 IQBC. All rights reserved.
//

import Foundation

class Category{
    
    var id: String?
    var icon: String?
    var name: String?
    
    init(id: String, icon: String, name: String) {
        
        self.id = id
        self.icon = icon
        self.name = name
        
    }
    
}
