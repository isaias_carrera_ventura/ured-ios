//
//  ServiceController.swift
//  Ured
//
//  Created by Isaias Carrera Ventura on 23/12/17.
//  Copyright © 2017 IQBC. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

class ServiceController {
    
    class func isRequestedCorrectly(_ dataResponse: DefaultDataResponse) -> Bool {
        
        guard dataResponse.error == nil else{
            
            return false
        }
        
        guard dataResponse.data != nil else{
            
            return false
        }

        return true
        
    }
    
    class func getServiceAcceptanceStatus(_ dataResponse: DefaultDataResponse) -> Bool? {
        
        guard dataResponse.error == nil else{
           
            return nil
        }
        
        guard dataResponse.data != nil else{
            return nil
        }
        
        return true
        
    }
    
    class func getServicePrice(_ dataResponse: DefaultDataResponse) -> Double? {
        
        guard dataResponse.error == nil else{
            
            return nil
        }
        
        guard let data = dataResponse.data else{
            
            return nil
        }
        
        let doubleJson = JSON(data: data)
        let double = doubleJson["cost"].doubleValue
        return double
        
    }
    
    class func getServiceListPartner(_ dataResponse: DefaultDataResponse, _ urgent: Bool) -> [ServiceModel]?{
        
        guard dataResponse.error == nil else{
            
            return nil
        }
        
        guard let data = dataResponse.data else{
            
            return nil
        }
        
        let json = JSON(data: data)
        let jsonServices = json["services"]
        var arrayServices = [ServiceModel]()
        
        for (_,subJson):(String,JSON) in jsonServices{
            
            let service = ServiceModel()
            service.idService = subJson["id"].stringValue
            service.description = subJson["description"].stringValue
            service.accepted = subJson["accepted"].boolValue
            let partnerJson = subJson["partner"]
            //PARTNER
            let partner = PartnerModel()
            partner.imageUrl = partnerJson["profile_image"].stringValue
            partner.partnerId = partnerJson["id"].stringValue
            partner.lastname = partnerJson["lastname"].stringValue
            partner.email = partnerJson["email"].stringValue
            partner.ranking = partnerJson["ranking"].intValue
            partner.name = partnerJson["name"].stringValue
            service.partnerModel = partner
            //PARTNER
            service.express = subJson["express"].boolValue
            service.quoted = subJson["quoted"].boolValue
            service.rejected = subJson["rejected"].boolValue
            //ADDRESS
            let addressJson = subJson["address"]
            let location = LocationUser()
            location.idLocation = addressJson["id"].stringValue
            location.latitude = addressJson["latitude"].doubleValue
            location.longitude = addressJson["longitude"].doubleValue
            location.name = addressJson["name"].stringValue
            location.address = addressJson["address"].stringValue
            service.location = location
            //ADDRES
            service.date = subJson["date"].stringValue
            service.paid = subJson["payed"].boolValue
            service.status = subJson["status"].boolValue
            service.hour = subJson["hour"].stringValue
            
            //USer
            let user = UserModel()
            let userJson = subJson["user"]
            user.imageUrl = userJson["profile_image"].stringValue
            user.userId = userJson["id"].stringValue
            user.lastname = userJson["lastname"].stringValue
            user.email = userJson["email"].stringValue
            user.ranking = userJson["ranking"].intValue
            user.name = userJson["name"].stringValue
            
            service.userModel = user
            
            
            if urgent == service.express {
             
                arrayServices.append(service)
                
            }
            
            
        }
        
        return arrayServices

        
    }
    
    class func getServiceListPartner(_ dataResponse: DefaultDataResponse) -> [ServiceModel]?{
        
        guard dataResponse.error == nil else{
            
            return nil
        }
        
        guard let data = dataResponse.data else{
            
            return nil
        }
        
        let json = JSON(data: data)
        let jsonServices = json["services"]
        var arrayServices = [ServiceModel]()
        
        for (_,subJson):(String,JSON) in jsonServices{
            
            let service = ServiceModel()
            service.idService = subJson["id"].stringValue
            service.description = subJson["description"].stringValue
            service.accepted = subJson["accepted"].boolValue
            let partnerJson = subJson["partner"]
            //PARTNER
            let partner = PartnerModel()
            partner.imageUrl = partnerJson["profile_image"].stringValue
            partner.partnerId = partnerJson["id"].stringValue
            partner.lastname = partnerJson["lastname"].stringValue
            partner.email = partnerJson["email"].stringValue
            partner.ranking = partnerJson["ranking"].intValue
            partner.name = partnerJson["name"].stringValue
            service.partnerModel = partner
            //PARTNER
            service.express = subJson["express"].boolValue
            service.quoted = subJson["quoted"].boolValue
            service.rejected = subJson["rejected"].boolValue
            //ADDRESS
            let addressJson = subJson["address"]
            let location = LocationUser()
            location.idLocation = addressJson["id"].stringValue
            location.latitude = addressJson["latitude"].doubleValue
            location.longitude = addressJson["longitude"].doubleValue
            location.name = addressJson["name"].stringValue
            location.address = addressJson["address"].stringValue
            service.location = location
            //ADDRES
            service.date = subJson["date"].stringValue
            service.paid = subJson["payed"].boolValue
            service.status = subJson["status"].boolValue
            service.hour = subJson["hour"].stringValue
            
            //USer
            let user = UserModel()
            let userJson = subJson["user"]
            user.imageUrl = userJson["profile_image"].stringValue
            user.userId = userJson["id"].stringValue
            user.lastname = userJson["lastname"].stringValue
            user.email = userJson["email"].stringValue
            user.ranking = userJson["ranking"].intValue
            user.name = userJson["name"].stringValue
            
            service.userModel = user
            
            arrayServices.append(service)
                        
        }
        
        return arrayServices
        
        
    }
    
    class func getServiceListUserByStatus(_ dataResponse: DefaultDataResponse, _ status: Bool) -> [ServiceModel]? {
    
        guard dataResponse.error == nil else{
            return nil
        }
        
        guard let data = dataResponse.data else{
            
            return nil
        }
        
        let json = JSON(data: data)
        let jsonServices = json["services"]
        var arrayServices = [ServiceModel]()
        
        for (_,subJson):(String,JSON) in jsonServices{
        
            let service = ServiceModel()
            service.idService = subJson["id"].stringValue
            service.description = subJson["description"].stringValue
            service.accepted = subJson["accepted"].boolValue
            let partnerJson = subJson["partner"]
            //PARTNER
            let partner = PartnerModel()
            partner.imageUrl = partnerJson["profile_image"].stringValue
            partner.partnerId = partnerJson["id"].stringValue
            partner.lastname = partnerJson["lastname"].stringValue
            partner.email = partnerJson["email"].stringValue
            partner.ranking = partnerJson["ranking"].intValue
            partner.name = partnerJson["name"].stringValue
            service.partnerModel = partner
            //PARTNER
            service.express = subJson["express"].boolValue
            service.quoted = subJson["quoted"].boolValue
            service.rejected = subJson["rejected"].boolValue
            //ADDRESS
            let addressJson = subJson["address"]
            let location = LocationUser()
            location.idLocation = addressJson["id"].stringValue
            location.latitude = addressJson["latitude"].doubleValue
            location.longitude = addressJson["longitude"].doubleValue
            location.name = addressJson["name"].stringValue
            location.address = addressJson["address"].stringValue
            service.location = location
            //ADDRES
            service.date = subJson["date"].stringValue
            service.paid = subJson["payed"].boolValue
            service.status = subJson["status"].boolValue
            service.hour = subJson["hour"].stringValue
            
            if service.status == status {
                arrayServices.append(service)
            }
            
        }
        
        return arrayServices
        
    }
    
    
}
