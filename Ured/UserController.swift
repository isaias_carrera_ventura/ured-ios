//
//  UserController.swift
//  Ured
//
//  Created by Isaias Carrera Ventura on 06/12/17.
//  Copyright © 2017 IQBC. All rights reserved.
//

import Foundation
import RealmSwift
import Alamofire
import SwiftyJSON

class UserController {
    
    class func saveUserSession(_ userModel: UserModel) {
        
        let realm = try! Realm()
        try! realm.write {
            
            realm.add(userModel)
            
        }
        
    }
    
    class func updateSession(_ image: String, _ name: String, _ lastname: String){
        
        let user = UserController.getUserSession()!
        
        let realm = try! Realm()
        try! realm.write {
            
            user.imageUrl = image
            user.name = name
            user.lastname = lastname
            
        }

        
    }
    
    class func removeUserSession(){
        
        let realm = try! Realm()
        
        try! realm.write {
            realm.deleteAll()
        }
        
        RedManager.removeStoredValues()
    }
    
    
    class func getUserSession() -> UserModel?{
        
        let realm = try! Realm()
        let users = realm.objects(UserModel.self)
        
        if users.count > 0{
            
            return users[0]
            
        }
        
        return nil
        
    }
    
    
    class func getUserSessionResponse(_ dataResponse: DefaultDataResponse) -> UserModel? {
        
        guard dataResponse.error == nil else{
            return nil
        }
        
        guard let data = dataResponse.data else{
            return nil
        }
        
        let json = JSON(data:data)
        let user = UserModel()
        let account = json["account"].stringValue
        let userId = json["user"].stringValue
        let jwt = json["jwt"].stringValue
        
        user.account = account
        user.userId = userId
        user.jwt = jwt
        
        return user
        
    }
    
    class func getUserSignUpResponse(_ dataResponse: DefaultDataResponse) -> UserModel? {
        
        guard dataResponse.error == nil else{
                       return nil
        }
        
        guard let data = dataResponse.data else{
            
            return nil
        }
        
        let json = JSON(data:data)
        let user = UserModel()
        let account = json["account"].stringValue
        let userId = json["userId"].stringValue
        let jwt = json["jwt"].stringValue
        
        user.account = account
        user.userId = userId
        user.jwt = jwt
        
        return user
        
    }
    
    class func getUserFromResponse(_ dataResponse: DefaultDataResponse) -> UserModel? {
        
        guard dataResponse.error == nil else{
            return nil
        }
        
        guard let data = dataResponse.data else{
            return nil
        }
        
        let json = JSON(data:data)
        let user = UserModel()
        
        let name = json["name"].stringValue
        let lastname = json["lastname"].stringValue
        let profileImage = json["profile_image"].stringValue
        let email = json["email"].stringValue
        let ranking = json["ranking"].intValue
        
        user.name = name
        user.lastname = lastname
        user.imageUrl = profileImage
        user.email = email
        user.ranking = ranking
        
        return user
        
    }
    
    class func getUserLocationList(_ userModel: UserModel){
        
        let headers : HTTPHeaders = ["Authorization" : "JWT \(userModel.value(forKey: "jwt") as! String)"]
        let url = Constants.ADDRESS_ENDPOINT + (userModel.value(forKey: "userId") as! String)
        
        Alamofire.request(url, method: .get,  headers: headers)
            .validate()
            .response { (dataResponse) in
                
                if let locationList = LocationController.getListFromResponse(dataResponse){
                 
                    LocationController.saveListLocation(locationList)
                    
                }
        }
        
    }
    
}
