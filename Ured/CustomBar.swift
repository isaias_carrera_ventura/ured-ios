//
//  CustomBar.swift
//  Ured
//
//  Created by Isaias Carrera Ventura on 29/11/17.
//  Copyright © 2017 IQBC. All rights reserved.
//

import Foundation
import UIKit
import Kingfisher

class CustomBar{
    
    static var viewControllerCurrent: UIViewController!
    
    static func setNavBarProfile(_ navBar: UINavigationBar,navItem: UINavigationItem, nav : UIViewController) -> Void{
        
        SetCenteredImage(navItem)
        SetUserImage(navItem)
        viewControllerCurrent = nav
        
    }
    
    static func setNavBar(_ navBar: UINavigationBar,navItem: UINavigationItem, nav : UIViewController) -> Void{
        
        SetCenteredImage(navItem)
        viewControllerCurrent = nav
        
    }
    
    
    static func SetCenteredImage(_ navItem: UINavigationItem){
        
        let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 40, height: 35))
        imageView.contentMode = .scaleAspectFit
        let image = UIImage(named: "logo_banner")
        imageView.image = image
        navItem.titleView = imageView
        
    }
    
    static func SetUserImage(_ navItem: UINavigationItem){
        
        
        var loadedImage = UIImage(named: "profile_icon")
        
        let imageString = RedManager.getSessionType() == Constants.userType ? (UserController.getUserSession()?.value(forKey: "imageUrl") as! String) : (PartnerController.getPartnerSession()?.value(forKey: "imageUrl") as! String)
        
        if let url = URL(string: imageString){
            
            KingfisherManager.shared.retrieveImage(with: url, options: [.transition(.fade(1)),.forceRefresh], progressBlock: nil, completionHandler: { image, error, cacheType, imageURL in
                
                if error == nil {
                    
                    let imageResize = ImageUtils.resizeImage(image: image!, targetSize: CGSize(width: 34, height: 34))
                    loadedImage = imageResize
                    
                }
                
                
                let imgWidth = 34
                let imgHeight = 34
                let button = UIButton(frame: CGRect(x: 0, y: 0, width: imgWidth, height: imgHeight))
                button.layer.cornerRadius = 0.5 * button.bounds.size.width
                button.clipsToBounds = true
                button.layer.borderWidth = 1.5
                button.layer.borderColor = ColorUtils.getGrayLightColorApp(1.0).cgColor
                button.setBackgroundImage(loadedImage, for: UIControlState())
                button.addTarget(self, action: #selector(CustomBar.Profile), for: UIControlEvents.touchUpInside)
                navItem.leftBarButtonItem = UIBarButtonItem(customView: button)
                
            })
            
            
        }
       
        
    }
    
    
    @objc static func Profile(){
        
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        
        if RedManager.isSessionInit() {
            
            if RedManager.getSessionType() == Constants.userType{
                
                let nextViewController = storyBoard.instantiateViewController(withIdentifier: "ProfileUser") as! ProfileUserViewController
                self.viewControllerCurrent.navigationController?.pushViewController(nextViewController, animated: true)
                
            }else{
                
                let nextViewController = storyBoard.instantiateViewController(withIdentifier: "ProfilePartner") as! ProfilePartnerViewController
                self.viewControllerCurrent.navigationController?.pushViewController(nextViewController, animated: true)
                
            }
            
        }
        
    }
    
    
}
