//
//  ProfilePartnerViewController.swift
//  Ured
//
//  Created by Isaias Carrera Ventura on 30/12/17.
//  Copyright © 2017 IQBC. All rights reserved.
//

import UIKit
import SwiftyButton
import Kingfisher

class ProfilePartnerViewController: UIViewController {
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var userEmail: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        CustomBar.setNavBar((self.navigationController?.navigationBar)!, navItem: navigationItem,nav: self)
        ImageUtils.circularImage(imageView, color: ColorUtils.getColorUredApp(1.0).cgColor)
        
        userName.text = (PartnerController.getPartnerSession()?.value(forKey: "name") as! String) + " " + (PartnerController.getPartnerSession()?.value(forKey: "lastname") as! String)
        userEmail.text = PartnerController.getPartnerSession()?.value(forKey: "email") as? String
        
        let imageString = PartnerController.getPartnerSession()?.value(forKey: "imageUrl") as! String
        if let url = URL(string: imageString){
            
            KingfisherManager.shared.retrieveImage(with: url, options: [.transition(.fade(1))], progressBlock: nil, completionHandler: { image, error, cacheType, imageURL in
                
                if error == nil {
                    
                    self.imageView.image = image!
                    
                }
            })
            
        }
        

        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func closeSession(_ sender: Any) {
        
        //Chaining alerts with messages on button click
        SweetAlert().showAlert("Cerrar sesión", subTitle: "¿Deseas cerrar sesión?", style: AlertStyle.warning, buttonTitle:"Cancelar", buttonColor: ColorUtils.getColorUredApp(1.0) , otherButtonTitle:  "Salir", otherButtonColor:ColorUtils.getGrayColorApp(1.0)) { (isOtherButton) -> Void in
            if isOtherButton == true {
            }
            else {
                
                PartnerController.removePartnerSessionSession()
                self.performSegue(withIdentifier: "closePartnerSession", sender: self)
                
            }
        }
        
    }
    
    @IBOutlet weak var changePassword: FlatButton!
    
    @IBAction func changePasswordAction(_ sender: Any) {
        
        self.performSegue(withIdentifier: "passwordChangeSegue", sender: self)
        
    }
    
    @IBAction func goToTermsAndConditions(_ sender: Any) {
        
        guard let url = URL(string: Constants.TERMS_ENDPOINT) else {
            return //be safe
        }
        
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        } else {
            UIApplication.shared.openURL(url)
        }
        
    }

}
