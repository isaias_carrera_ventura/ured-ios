//
//  PartnerController.swift
//  Ured
//
//  Created by Isaias Carrera Ventura on 06/12/17.
//  Copyright © 2017 IQBC. All rights reserved.
//

import Foundation
import RealmSwift
import SwiftyJSON
import Alamofire

class PartnerController{
    
    
    class func saveUserSession(_ partner: PartnerModel) {
        
        let realm = try! Realm()
        try! realm.write {
            
            realm.add(partner)
            
        }
        
    }
    
    class func removePartnerSessionSession(){
        
        let realm = try! Realm()
        
        try! realm.write {
            realm.deleteAll()
        }
        
        RedManager.removeStoredValues()
    }
    

    
    
    class func getPartnerSignUpResponse(_ dataResponse: DefaultDataResponse) -> PartnerModel? {
        
        guard dataResponse.error == nil else{
            return nil
        }
        
        guard let data = dataResponse.data else{
            return nil
        }
        
        let json = JSON(data:data)
        let user = PartnerModel()
        let account = json["account"].stringValue
        let userId = json["partnerId"].stringValue
        let jwt = json["jwt"].stringValue
        
        user.account = account
        user.partnerId = userId
        user.jwt = jwt
        
        return user
        
    }

    
    class func getPartnerSession() -> PartnerModel?{
        
        let realm = try! Realm()
        let users = realm.objects(PartnerModel.self)
        
        if users.count > 0{
            
            return users[0]
            
        }
        
        return nil
        
    }

    
    class func getListForPartners(_ dataResponse: DefaultDataResponse) -> [PartnerModel]? {
        
        guard dataResponse.error == nil else{
            return nil
        }
        
        guard let data = dataResponse.data else{
            return nil
        }
        
        let json = JSON(data:data)
        let jsonPartner = json["partners"]
        var partnerList = [PartnerModel]()
        
        for (_,subJson):(String,JSON) in jsonPartner{
            
            let account = subJson["account"].stringValue
            let partnerId = subJson["partnerId"].stringValue
            let rfc = subJson["rfc"].stringValue
            let name = subJson["name"].stringValue
            let lastname = subJson["lastname"].stringValue
            let email = subJson["email"].stringValue
            let category = subJson["category"].stringValue
            let imageUrl = subJson["profile_image"].stringValue
            let ranking = subJson["ranking"].intValue
            let latitude = subJson["latitude"].doubleValue
            let longitude = subJson["longitude"].doubleValue
            
            let isBusiness = subJson["isBusiness"].boolValue
            
            
            let partner = PartnerModel()
            partner.account = account
            partner.partnerId = partnerId
            partner.rfc = rfc
            partner.name = name
            partner.lastname = lastname
            partner.email = email
            partner.category = category
            partner.imageUrl = imageUrl
            partner.ranking = ranking
            partner.latitude = latitude
            partner.longitude = longitude
            
            let idBusiness = subJson["myPartner"].stringValue
            
            if !isBusiness && idBusiness != "" {
                
                let jsonArray = subJson["myPartner_info"]
                
                let jsonObject = jsonArray[0]["profile_image"].stringValue
                
                partner.isBusiness = isBusiness ? true : false
                partner.idBusiness = idBusiness
                partner.imageBusiness = jsonObject
                
                
            }else{
                
                partner.isBusiness = true
                partner.imageBusiness = ""
                partner.idBusiness = ""
                
            }
            
            partnerList.append(partner)
            
        }
        
        return partnerList
        
    }
    
    
    class func getPartnerSessionResponse(_ dataResponse: DefaultDataResponse) -> PartnerModel? {
        
        guard dataResponse.error == nil else{
            
            return nil
        }
        
        guard let data = dataResponse.data else{
            return nil
        }
        
        let json = JSON(data:data)
        
        let user = PartnerModel()
        let account = json["account"].stringValue
        let userId = json["partner"].stringValue
        let jwt = json["jwt"].stringValue
        
        user.account = account
        user.partnerId = userId
        user.jwt = jwt
        
        return user
        
    }
    
    
    class func getPartnerFromResponse(_ dataResponse: DefaultDataResponse) -> PartnerModel? {
        
        guard dataResponse.error == nil else{
            
            return nil
        }
        
        guard let data = dataResponse.data else{
            return nil
        }
        
        
        let json = JSON(data:data)
        let partner = PartnerModel()
        
        partner.account = json["account"].stringValue
        partner.partnerId = json["partnerId"].stringValue
        partner.name = json["name"].stringValue
        partner.lastname = json["lastname"].stringValue
        partner.email = json["email"].stringValue
        partner.rfc = json["rfc"].stringValue
        partner.imageUrl = json["profile_image"].stringValue
        partner.ranking = json["ranking"].intValue
        partner.latitude = json["latitude"].doubleValue
        partner.longitude = json["longitude"].doubleValue
        partner.category = json["category"].stringValue
        
        return partner
        
    }
    

    
}
