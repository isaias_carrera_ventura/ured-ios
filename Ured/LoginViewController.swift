//
//  LoginViewController.swift
//  Ured
//
//  Created by Isaias Carrera Ventura on 29/11/17.
//  Copyright © 2017 IQBC. All rights reserved.
//
import UIKit
import Alamofire
import CryptoSwift
import SwiftValidator
import Firebase
import NVActivityIndicatorView

class LoginViewController: UIViewController , UITextFieldDelegate, NVActivityIndicatorViewable, ValidationDelegate{
    
    @IBOutlet weak var navItem: UINavigationItem!
    @IBOutlet weak var navBar: UINavigationBar!
    
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    
    var loginType : Int = 0
    
    let validator = Validator()
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        CustomBar.setNavBar((self.navBar)!, navItem: navItem,nav: self)
        
        validator.registerField(emailTextField, rules: [RequiredRule(),EmailRule()])
        validator.registerField(passwordTextField, rules: [RequiredRule(),MinLengthRule(length: 6)])
        
        emailTextField.roundBorder()
        passwordTextField.roundBorder()
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func signUpRED(_ sender: Any) {
        
        if loginType == Constants.userType{
            
            self.performSegue(withIdentifier: "signUpUserSegue", sender: self)
            
        }else if loginType == Constants.partnerType {
            
            self.performSegue(withIdentifier: "signUpPartnerSegue", sender: self)
            
        }
        
    }
    
    @IBAction func loginRED(_ sender: Any) {
        
        validator.validate(self)
        
    }
    
    func validationSuccessful() {
        
        if loginType == Constants.userType{
            
            //self.performSegue(withIdentifier: "mainUserSegue", sender: self)
            loginUser()
            
            
        }else if loginType == Constants.partnerType {
            
            loginPartner()
            //self.performSegue(withIdentifier: "mainPartnerSegue", sender: self)
            
        }
        
    }
    
    func validationFailed(_ errors:[(Validatable ,ValidationError)]) {
        
        // turn the fields to red
        for (field, error) in errors {
            if let field = field as? UITextField {
                field.layer.borderColor = UIColor.red.cgColor
                field.layer.borderWidth = 1.0
            }
            error.errorLabel?.text = error.errorMessage // works if you added labels
            error.errorLabel?.isHidden = false
        }
        
        self.view.endEditing(true)
        
        _ = SweetAlert().showAlert("Error", subTitle: "¡Ingresa los datos requeridos! ", style: AlertStyle.error, buttonTitle:"Ok", buttonColor:ColorUtils.getGreenColorApp(1.0))
        

        
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        textField.resignFirstResponder()
        return true
        
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        self.view.endEditing(true)
        
    }
    
    @IBAction func cancelAction(_ sender: Any) {
        
        self.dismiss(animated: true, completion: nil)
        
    }
    
    func loginPartner() -> Void {
        self.startAnimating(Constants.getSizeForLoadingView(view: self.view), type: NVActivityIndicatorType.ballBeat, color: ColorUtils.getColorUredApp(1.0), padding: NVActivityIndicatorView.DEFAULT_PADDING)
        
        let password = passwordTextField.text!.sha1()
        let params : Parameters = ["email":emailTextField.text!, "password": password,"isPartner": "true", "isUser": "false"]
        
        Alamofire.request(Constants.LOGIN_ENDPOINT, method: .post, parameters: params)
            .validate()
            .response { (dataResponse) in
                
                self.stopAnimating()
                
                if let partner = PartnerController.getPartnerSessionResponse(dataResponse){
                    
                    self.getPartnerInformation(partner)
                    self.configRemoteNotifications((partner.value(forKey: "partnerId") as! String),(partner.value(forKey: "jwt") as! String), Constants.partnerType)
                    
                }else{
                    
                    //TODO: Handle error
                    
                    self.view.endEditing(true)
                    
                    _ = SweetAlert().showAlert("¡Algo salió mal!", subTitle: "¡Verifica tus datos y checa tu conexión a internet!", style: AlertStyle.error, buttonTitle:"Ok", buttonColor:ColorUtils.getGreenColorApp(1.0))
                    
                }
                
        }
        
    }
    
    func loginUser() -> Void {
        
        self.startAnimating(Constants.getSizeForLoadingView(view: self.view), type: NVActivityIndicatorType.ballBeat, color: ColorUtils.getColorUredApp(1.0), padding: NVActivityIndicatorView.DEFAULT_PADDING)
        
        let password = passwordTextField.text!.sha1()
        let params : Parameters = ["email":emailTextField.text!, "password": password,"isPartner": "false", "isUser": "true"]
        
        Alamofire.request(Constants.LOGIN_ENDPOINT, method: .post, parameters: params)
            .validate()
            .response { (dataResponse) in
                
                self.stopAnimating()
                
                if let user = UserController.getUserSessionResponse(dataResponse){
                    
                    self.configRemoteNotifications((user.value(forKey: "userId") as! String),(user.value(forKey: "jwt") as! String), Constants.userType)
                    
                    self.getUserInformation(user)
                    
                }else{
                    
                    //TODO: Handle error
                    
                    self.view.endEditing(true)
                    
                    _ = SweetAlert().showAlert("¡Algo salió mal!", subTitle: "¡Verifica tus datos y checa tu conexión a internet!", style: AlertStyle.error, buttonTitle:"Ok", buttonColor:ColorUtils.getGreenColorApp(1.0))
                    
                }
                
        }
    }
    
    func configRemoteNotifications(_ idAccount: String, _ jwt: String, _ typeAccount: Int) {
        
        if let refreshedToken = InstanceID.instanceID().token(){
            
            let headers : HTTPHeaders = ["Authorization" : "JWT \(jwt)"]
            let params: [String:String] = ["id":idAccount, "device":refreshedToken]
            
            var url = ""
            
            if typeAccount == Constants.userType {
                
                url = Constants.DEVICE_USER_ENDPOINT
                
            }else{
                
                url = Constants.DEVICE_PARTNER_ENDPOINT
                
            }
            
            Alamofire.request(url, method: .post, parameters: params, headers: headers).validate()
                .response(completionHandler: { (reponse) in
                    
                    
                    guard reponse.error == nil else{
                        return
                        
                    }
                    
                    guard let dataResponse = reponse.data else{
                        return
                        
                    }
                    
                    let jsonResponseBusiness = try! JSONSerialization.jsonObject(with: dataResponse, options: .allowFragments)
                    
                    guard jsonResponseBusiness is [String:Any] else{
                        
                        return
                        
                    }
                    
                    
                })
            
        }
        
    }
    
    
    func getPartnerInformation(_ user: PartnerModel){
        
        self.startAnimating(Constants.getSizeForLoadingView(view: self.view), type: NVActivityIndicatorType.ballBeat, color: ColorUtils.getColorUredApp(1.0), padding: NVActivityIndicatorView.DEFAULT_PADDING)
        
        let headers : HTTPHeaders = ["Authorization" : "JWT \(user.value(forKey: "jwt") as! String)"]
        let url = Constants.PARTNER_ENDPOINT + (user.value(forKey: "partnerId") as! String)
        
        Alamofire.request(url, method: .get,  headers: headers)
            .validate()
            .response { (dataResponse) in
                
                self.stopAnimating()
                
                if let partnerSave = PartnerController.getPartnerFromResponse(dataResponse){
                    
                    partnerSave.jwt = user.value(forKey: "jwt") as? String
                    PartnerController.saveUserSession(partnerSave)
                    RedManager.setSessionType(Constants.partnerType)
                    
                    self.performSegue(withIdentifier: "mainPartnerSegue", sender: self)
                    
                }else{
                    
                    //TODO: Handle error
                    _ = SweetAlert().showAlert("¡Algo salió mal!", subTitle: "¡Verifica tus datos y checa tu conexión a internet!", style: AlertStyle.error, buttonTitle:"Ok", buttonColor:ColorUtils.getGreenColorApp(1.0))
                    
                    
                }
                
        }
        
    }
    
    func getUserInformation(_ user: UserModel){
        
        self.startAnimating(Constants.getSizeForLoadingView(view: self.view), type: NVActivityIndicatorType.ballBeat, color: ColorUtils.getColorUredApp(1.0), padding: NVActivityIndicatorView.DEFAULT_PADDING)
        
        let headers : HTTPHeaders = ["Authorization" : "JWT \(user.value(forKey: "jwt") as! String)"]
        let url = Constants.USER_ENDPOINT + (user.value(forKey: "userId") as! String)
        
        Alamofire.request(url, method: .get,  headers: headers)
            .validate()
            .response { (dataResponse) in
                
                self.stopAnimating()
                
                if let userSave = UserController.getUserFromResponse(dataResponse){
                    
                    //TODO: Save user session
                    userSave.account = user.value(forKey: "account") as? String
                    userSave.jwt = user.value(forKey: "jwt") as? String
                    userSave.userId = user.value(forKey: "userId") as? String
                    
                    UserController.saveUserSession(userSave)
                    UserController.getUserLocationList(userSave)
                    
                    RedManager.setSessionType(Constants.userType)
                    self.performSegue(withIdentifier: "mainUserSegue", sender: self)
                    
                }else{
                    
                    _ = SweetAlert().showAlert("¡Algo salió mal!", subTitle: "¡Verifica tus datos y checa tu conexión a internet!", style: AlertStyle.error, buttonTitle:"Ok", buttonColor:ColorUtils.getGreenColorApp(1.0))
                    
                    
                }
                
        }
        
    }
    
    @IBAction func restorePassword(_ sender: Any) {
        
        self.performSegue(withIdentifier: "restorePasswordSegue", sender: self)
        
    }
    
    
}
