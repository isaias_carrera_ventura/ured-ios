//
//  PartnerModel.swift
//  Ured
//
//  Created by Isaias Carrera Ventura on 06/12/17.
//  Copyright © 2017 IQBC. All rights reserved.
//

import Foundation
import RealmSwift

class PartnerModel: Object{
    
    dynamic var account: String?
    dynamic var partnerId: String?
    dynamic var rfc,name,lastname,email,jwt: String?
    dynamic var category,imageUrl: String?
    dynamic var ranking = 0
    dynamic var latitude = 0.0
    dynamic var longitude = 0.0
    
    dynamic var isBusiness = true
    dynamic var idBusiness : String?
    dynamic var imageBusiness: String?
    
    
}
