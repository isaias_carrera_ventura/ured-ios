//
//  SignUpPartnerViewController.swift
//  Ured
//
//  Created by Isaias Carrera Ventura on 29/11/17.
//  Copyright © 2017 IQBC. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyButton
import NVActivityIndicatorView
import SwiftValidator
import M13Checkbox

class SignUpPartnerViewController: UIViewController, UITextFieldDelegate, getCategoryBackDelegate, NVActivityIndicatorViewable, ValidationDelegate {
    
    @IBOutlet weak var rfcEditText: UITextField!
    @IBOutlet weak var nameEditText: UITextField!
    @IBOutlet weak var lastnameEditText: UITextField!
    @IBOutlet weak var editTextEmail: UITextField!
    @IBOutlet weak var categoryButton: FlatButton!
    @IBOutlet weak var passwordEditText: UITextField!
    
    @IBOutlet weak var checkBox: M13Checkbox!
    @IBOutlet weak var scrollView: UIScrollView!
    
    @IBOutlet weak var navBar: UINavigationBar!
    @IBOutlet weak var navItem: UINavigationItem!
    
    var category: Category?
    var categoryList : [Category]?
    
    let validator = Validator()
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        CustomBar.setNavBar((self.navBar)!, navItem: navItem,nav: self)
        
        validator.registerField(nameEditText, rules: [RequiredRule()])
        validator.registerField(lastnameEditText, rules: [RequiredRule()])
        validator.registerField(editTextEmail, rules: [EmailRule()])
        validator.registerField(passwordEditText, rules: [RequiredRule(message: "Contraseña requerida"),MinLengthRule(length: 6, message:" Mínimo 6 caractéres")])
        
        validator.registerField(rfcEditText, rules: [RegexRule(regex: "[A-Z,Ñ,&]{3,4}[0-9]{2}[0-1][0-9][0-3][0-9][A-Z,0-9]?[A-Z,0-9]?[0-9,A-Z]?", message: "No es un RFC válido")])

        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        if textField == editTextEmail || textField == passwordEditText{
            
            scrollView.setContentOffset(CGPoint(x:0,y:250), animated: true)
        }
        
    }
    
    @IBAction func selectCategory(_ sender: Any) {
        
        //selectCategorySegue
        self.startAnimating(Constants.getSizeForLoadingView(view: self.view), type: NVActivityIndicatorType.ballBeat, color: ColorUtils.getColorUredApp(1.0), padding: NVActivityIndicatorView.DEFAULT_PADDING)
        
        Alamofire.request(Constants.CATEGORY_ENDPOINT, method: .get)
            .validate()
            .response { (dataResponse) in
                
                self.stopAnimating()
                
                let categoryListServer = CategoryController.getCategoryListFromResponse(dataResponse)
                
                if categoryListServer != nil {
                    
                    self.categoryList = categoryListServer!
                    self.performSegue(withIdentifier: "selectCategorySegue", sender: self)
                    
                }else{
                    
                    _ = SweetAlert().showAlert("Ops!", subTitle: "No pudimos obtener las categorías", style: AlertStyle.warning, buttonTitle:"Ok", buttonColor:ColorUtils.getGreenColorApp(1.0))
                    
                }
        }
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "selectCategorySegue"{
            
            let vc = segue.destination as! SelectorCategoryViewController
            vc.categoryList = self.categoryList!
            vc.delegate = self
            
        }
        
    }
    
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        scrollView.setContentOffset(CGPoint(x:0,y:0), animated: true)
        
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        textField.resignFirstResponder()
        return true
        
    }

    @IBAction func cancelAction(_ sender: Any) {
        
        self.dismiss(animated: true, completion: nil)
        
    }
    
    
    func writeCategoryBack(value: Category){
        
        categoryButton.setTitle(value.name, for: .normal)
        category = value
        
    }

    @IBAction func signUp(_ sender: Any) {
        
        //mainPartnerSegue
        validator.validate(self)
        
    }
    
    func validationSuccessful() {
        
        if self.category != nil {
        
            if self.checkBox.checkState == .checked {
                
                self.startAnimating(Constants.getSizeForLoadingView(view: self.view), type: NVActivityIndicatorType.ballBeat, color: ColorUtils.getColorUredApp(1.0), padding: NVActivityIndicatorView.DEFAULT_PADDING)
                let password = passwordEditText.text!.sha1()
                let params : Parameters = ["name":nameEditText.text!,
                                           "lastname": lastnameEditText.text!,
                                           "email": editTextEmail.text!,
                                           "password": password,
                                           "rfc":rfcEditText.text!,
                                           "id_category":self.category!.id!]
              
                Alamofire.request(Constants.SIGNUP_ENDPOINT, method: .post, parameters: params)
                    .validate()
                    .response { (dataResponse) in
                        
                        self.stopAnimating()
                        
                        if let partner = PartnerController.getPartnerSignUpResponse(dataResponse){
                            
                            self.getPartnerInformation(partner)
                            
                        }else{
                            
                            //TODO: Handle error
                            
                            _ = SweetAlert().showAlert("¡Algo salió mal!", subTitle: "¡Verifica tus datos y checa tu conexión a internet!", style: AlertStyle.error, buttonTitle:"Ok", buttonColor:ColorUtils.getGreenColorApp(1.0))
                            
                        }
                        
                }

                
            }else{
                
                _ = SweetAlert().showAlert("Ops", subTitle: "Acepta nuestros términos y condiciones.", style: AlertStyle.error, buttonTitle:"Ok", buttonColor:ColorUtils.getGreenColorApp(1.0))
                
                self.view.endEditing(true)
            }
            
        }else{
            
            
            _ = SweetAlert().showAlert("Ops", subTitle: "Selecciona una categoría para ofrecer tus servicios", style: AlertStyle.error, buttonTitle:"Ok", buttonColor:ColorUtils.getGreenColorApp(1.0))
            
            self.view.endEditing(true)
            
        }
        
    }
    
    func getPartnerInformation(_ user: PartnerModel){
        
        self.startAnimating(Constants.getSizeForLoadingView(view: self.view), type: NVActivityIndicatorType.ballBeat, color: ColorUtils.getColorUredApp(1.0), padding: NVActivityIndicatorView.DEFAULT_PADDING)
        
        let headers : HTTPHeaders = ["Authorization" : "JWT \(user.value(forKey: "jwt") as! String)"]
        let url = Constants.PARTNER_ENDPOINT + (user.value(forKey: "partnerId") as! String)
        
        Alamofire.request(url, method: .get,  headers: headers)
            .validate()
            .response { (dataResponse) in
                
                self.stopAnimating()
                
                if let partnerSave = PartnerController.getPartnerFromResponse(dataResponse){
                    
                    partnerSave.jwt = user.value(forKey: "jwt") as? String
                    PartnerController.saveUserSession(partnerSave)
                    RedManager.setSessionType(Constants.partnerType)
                    
                    self.performSegue(withIdentifier: "mainPartnerSegue", sender: self)
                                        
                }else{
                    
                    //TODO: Handle error
                    _ = SweetAlert().showAlert("¡Algo salió mal!", subTitle: "¡Verifica tus datos y checa tu conexión a internet!", style: AlertStyle.error, buttonTitle:"Ok", buttonColor:ColorUtils.getGreenColorApp(1.0))
                    
                }
                
        }
        
    }
    

    

    
    func validationFailed(_ errors:[(Validatable ,ValidationError)]) {
        
        var errorString = ""
        
        // turn the fields to red
        for (field, error) in errors {
            if let field = field as? UITextField {
                field.layer.borderColor = UIColor.red.cgColor
                field.layer.borderWidth = 1.0
            }
            errorString = errorString + "\n\(error.errorMessage)"
            error.errorLabel?.text = error.errorMessage // works if you added labels
            error.errorLabel?.isHidden = false
        }
        
        self.view.endEditing(true)
        
        _ = SweetAlert().showAlert("Error", subTitle: "¡Ingresa los datos requeridos!\n\(errorString)", style: AlertStyle.error, buttonTitle:"Ok", buttonColor:ColorUtils.getGreenColorApp(1.0))
        
        
        
    }
    
    @IBAction func terms(_ sender: Any) {
        
        guard let url = URL(string: Constants.TERMS_ENDPOINT) else {
            return //be safe
        }
        
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        } else {
            UIApplication.shared.openURL(url)
        }
        
        
    }
    
}
