//
//  UserServiceTableViewCell.swift
//  Ured
//
//  Created by Isaias Carrera Ventura on 30/11/17.
//  Copyright © 2017 IQBC. All rights reserved.
//

import UIKit
import Cosmos

class UserServiceTableViewCell: UITableViewCell {

    
    @IBOutlet weak var nameService: UILabel!
    @IBOutlet weak var dateService: UILabel!
    @IBOutlet weak var isPaid: UILabel!
    @IBOutlet weak var imageViewPartnerService: UIImageView!
    @IBOutlet weak var ranking: CosmosView!
    
}
