//
//  UserModel.swift
//  Ured
//
//  Created by Isaias Carrera Ventura on 06/12/17.
//  Copyright © 2017 IQBC. All rights reserved.
//

import Foundation
import RealmSwift

class UserModel: Object {
    
    dynamic var account : String?
    dynamic var userId: String?
    dynamic var name,lastname,email,jwt: String?
    dynamic var imageUrl: String?
    dynamic var ranking : Int = 0
    
}
