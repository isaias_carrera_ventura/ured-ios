//
//  UserLocationViewController.swift
//  Ured
//
//  Created by Isaias Carrera Ventura on 24/12/17.
//  Copyright © 2017 IQBC. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation
import Alamofire
import NVActivityIndicatorView
import SwiftValidator
import AddressBookUI

class UserLocationViewController: UIViewController, UITextFieldDelegate, NVActivityIndicatorViewable,ValidationDelegate{
    
    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var textFieldAddress: UITextField!
    @IBOutlet weak var textFieldDescription: UITextField!
    
    //Location variables
    var userLocation:CLLocation! = nil
    var locationManager:CLLocationManager! = nil
    
    let validator = Validator()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        CustomBar.setNavBar((self.navigationController?.navigationBar)!, navItem: navigationItem,nav: self)
        
        validator.registerField(textFieldAddress, rules: [RequiredRule()])
        validator.registerField(textFieldDescription, rules: [RequiredRule()])
        
        textFieldAddress.roundBorder()
        textFieldDescription.roundBorder()
        
        let uiLongPressGestureRecognizer = UILongPressGestureRecognizer(target: self, action: #selector(UserLocationViewController.mapViewLongPressed(gestureRecognizer:)))
        uiLongPressGestureRecognizer.minimumPressDuration = 2
        mapView.addGestureRecognizer(uiLongPressGestureRecognizer)
        
    }
    
    func mapViewLongPressed(gestureRecognizer: UIGestureRecognizer){
        
        let touchPoint = gestureRecognizer.location(in: self.mapView)
        let coordinate = mapView.convert(touchPoint, toCoordinateFrom: self.mapView)

        searchAddressString(coordinate)
        
    }
    
    func searchAddressString(_ coordinate: CLLocationCoordinate2D){
        
        let location = CLLocation(latitude: coordinate.latitude, longitude: coordinate.longitude)
        
        self.startAnimating(Constants.getSizeForLoadingView(view: self.view), type: NVActivityIndicatorType.ballBeat, color: ColorUtils.getColorUredApp(1.0), padding: NVActivityIndicatorView.DEFAULT_PADDING)
        
        CLGeocoder().reverseGeocodeLocation(location, completionHandler: {(placemarks, error) -> Void in
            
            self.stopAnimating()
            
            if error != nil {
                
                _ = SweetAlert().showAlert("Ops!", subTitle: "Algo salió mal", style: AlertStyle.error, buttonTitle:"Ok", buttonColor:ColorUtils.getGreenColorApp(1.0))
                return
            
            }
            
            if placemarks!.count > 0 {
                
                let pm = placemarks![0]
                let address = ABCreateStringWithAddressDictionary(pm.addressDictionary!, false)
                self.textFieldAddress.text = address
                self.updateLocation(location)
                
            }
        })
        
    }
    
    func searchLocationByString(_ address: String) {
        
        self.startAnimating(Constants.getSizeForLoadingView(view: self.view), type: NVActivityIndicatorType.ballBeat, color: ColorUtils.getColorUredApp(1.0), padding: NVActivityIndicatorView.DEFAULT_PADDING)
        
        CLGeocoder().geocodeAddressString(address, completionHandler: { (placemarks, error) in
            
            self.stopAnimating()
            
            if error != nil {
                
                _ = SweetAlert().showAlert("Ops!", subTitle: "Algo salió mal", style: AlertStyle.error, buttonTitle:"Ok", buttonColor:ColorUtils.getGreenColorApp(1.0))
                
                return
            }
            
            if placemarks!.count > 0 {
                if let placemark = placemarks?[0] {
                    
                    let location = placemark.location
                    self.updateLocation(location!)
                    
                }
                
            }
        })
        
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func saveLocation(_ sender: Any) {
        
        validator.validate(self)
        
    }
    
    func validationSuccessful() {
       
        if self.userLocation != nil {
            
            let headers : HTTPHeaders = ["Authorization" : "JWT \(UserController.getUserSession()!.value(forKey: "jwt") as! String)"]
            let params : Parameters = ["name":textFieldDescription.text!, "address":textFieldAddress.text!,"latitude": self.userLocation.coordinate.latitude, "longitude": self.userLocation.coordinate.longitude,"user":"\(UserController.getUserSession()?.value(forKey: "userId") as! String)"]
            
            
            self.startAnimating(Constants.getSizeForLoadingView(view: self.view), type: NVActivityIndicatorType.ballBeat, color: ColorUtils.getColorUredApp(1.0), padding: NVActivityIndicatorView.DEFAULT_PADDING)
            
            Alamofire.request(Constants.ADDRESS_ENDPOINT, method: .post, parameters: params, headers: headers).validate()
            .response(completionHandler: { (dataResponse) in
                
                self.stopAnimating()
                
                if let location = LocationController.getAddressFromResponse(dataResponse){
                    
                    LocationController.saveLocation(location)
                    
                    _ = SweetAlert().showAlert("Ubicación guardada", subTitle: "Agregaste una ubicación correctamente.", style: .success, buttonTitle: "Ok", action: { (flag) in
                        
                        self.navigationController?.popViewController(animated: true)
                        
                    })
                    
                }else{
                    
                    _ = SweetAlert().showAlert("Error", subTitle: "¡Algo salió mal! ", style: AlertStyle.error, buttonTitle:"Ok", buttonColor:ColorUtils.getGreenColorApp(1.0))
                }
                
            })
            
            
        }else{
            
            _ = SweetAlert().showAlert("Error", subTitle: "Selecciona una ubicación en el mapa, dejándo presionado sobre la ubicación", style: AlertStyle.error, buttonTitle:"Ok", buttonColor:ColorUtils.getGreenColorApp(1.0))
        
            
        }
        
    }
    
    func validationFailed(_ errors:[(Validatable ,ValidationError)]) {
        
        // turn the fields to red
        for (field, error) in errors {
            if let field = field as? UITextField {
                field.layer.borderColor = UIColor.red.cgColor
                field.layer.borderWidth = 1.0
            }
            error.errorLabel?.text = error.errorMessage // works if you added labels
            error.errorLabel?.isHidden = false
        }
        
        self.view.endEditing(true)
        
        _ = SweetAlert().showAlert("Error", subTitle: "¡Ingresa los datos requeridos! ", style: AlertStyle.error, buttonTitle:"Ok", buttonColor:ColorUtils.getGreenColorApp(1.0))
        
        
        
    }
    
    func updateLocation(_ location: CLLocation){
        
        self.userLocation = location
        
        self.mapView.removeAnnotations(self.mapView.annotations)

        let span = MKCoordinateSpanMake(0, 360 / pow(2, Double(10)) * Double(self.view.frame.size.width) / 256)
        self.mapView.setRegion(MKCoordinateRegionMake(location.coordinate, span), animated: true)
        
        let annotation = MKPointAnnotation()
        
        annotation.coordinate = CLLocationCoordinate2D(latitude: location.coordinate.latitude, longitude: location.coordinate.longitude)
        
        self.mapView.addAnnotation(annotation)
        
    }
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        textField.resignFirstResponder()
        return true
        
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        self.view.endEditing(true)
        
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        if textField == self.textFieldAddress{
            
            searchLocationByString(textField.text!)
            
        }
        
    }

}
