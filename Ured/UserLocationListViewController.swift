//
//  UserLocationListViewController.swift
//  Ured
//
//  Created by Isaias Carrera Ventura on 24/12/17.
//  Copyright © 2017 IQBC. All rights reserved.
//

import UIKit
import Alamofire
import NVActivityIndicatorView

class UserLocationListViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, NVActivityIndicatorViewable {

    @IBOutlet weak var tableView: UITableView!
    
    var listArray = [LocationUser]()
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        CustomBar.setNavBar((self.navigationController?.navigationBar)!, navItem: navigationItem,nav: self)
        
        
        self.navigationItem.setRightBarButton(UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(UserLocationListViewController.addLocation)), animated: true)

        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        
        let headers : HTTPHeaders = ["Authorization" : "JWT \(UserController.getUserSession()!.value(forKey: "jwt") as! String)"]
        let url = Constants.ADDRESS_ENDPOINT + (UserController.getUserSession()!.value(forKey: "userId") as! String)
    
        self.startAnimating(Constants.getSizeForLoadingView(view: self.view), type: NVActivityIndicatorType.ballBeat, color: ColorUtils.getColorUredApp(1.0), padding: NVActivityIndicatorView.DEFAULT_PADDING)
        
        Alamofire.request(url, method: .get,  headers: headers)
            .validate()
            .response { (dataResponse) in
                
                self.stopAnimating()
                
                if let locationList = LocationController.getListFromResponse(dataResponse){
                    
                    LocationController.saveRepeatedLocationList(locationList)
                    
                    self.listArray = LocationController.getLocationInDisk()
                    self.tableView.reloadData()
                    
                }else{
                    
                    self.listArray = LocationController.getLocationInDisk()
                    self.tableView.reloadData()
                    
                }
        }

        
    }
    
    func addLocation(){
        
        self.performSegue(withIdentifier: "addLocationSegue", sender: self)
    }

    override func didReceiveMemoryWarning() {
        
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
        
    }
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    
        return listArray.count
    
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath:
        IndexPath) {
        
        if editingStyle == .delete{
            
            let headers : HTTPHeaders = ["Authorization" : "JWT \(UserController.getUserSession()!.value(forKey: "jwt") as! String)"]
            
            let url = Constants.ADDRESS_ENDPOINT + (self.listArray[indexPath.row].value(forKey: "idLocation") as! String)
            
            self.startAnimating(Constants.getSizeForLoadingView(view: self.view), type: NVActivityIndicatorType.ballBeat, color: ColorUtils.getColorUredApp(1.0), padding: NVActivityIndicatorView.DEFAULT_PADDING)
            
            Alamofire.request(url, method: .delete,  headers: headers)
                .validate()
                .response { (dataResponse) in
                    
                    self.stopAnimating()
                    
                    if LocationController.getStatusResponseForDeletion(dataResponse){
                        
                        LocationController.removeLocation(self.listArray[indexPath.row])
                        self.listArray.remove(at: indexPath.row)
                        self.tableView.reloadData()
                        
                        _ = SweetAlert().showAlert("Ubicación borrada!", subTitle: "Borraste una ubicación exitosamente", style: AlertStyle.success, buttonTitle:"Ok", buttonColor:ColorUtils.getGreenColorApp(1.0))
                        
                    }else{
                        
                        _ = SweetAlert().showAlert("Ops!", subTitle: "Algo salió mal", style: AlertStyle.error, buttonTitle:"Ok", buttonColor:ColorUtils.getGreenColorApp(1.0))
                        
                    }
            }
            

        }
        
        
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        
        //CellBook
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! LocationTableViewCell
        cell.locationName.text = listArray[indexPath.row].value(forKey: "name") as? String
        cell.locationDescription.text = listArray[indexPath.row].value(forKey: "address") as? String
        return cell
        
    }
   
}
