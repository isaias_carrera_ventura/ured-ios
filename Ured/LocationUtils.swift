//
//  LocationUtils.swift
//  Ured
//
//  Created by Isaias Carrera Ventura on 24/12/17.
//  Copyright © 2017 IQBC. All rights reserved.
//

import CoreLocation
import UIKit
import Foundation

class LocationUtils {
    
    
    static func GetStatusLocation(_ locationManager: CLLocationManager,view: UIViewController) -> Bool{
        
        switch CLLocationManager.authorizationStatus() {
        case .authorizedAlways: break
        case .authorizedWhenInUse: break
        case .notDetermined:
            locationManager.requestAlwaysAuthorization()
        case .restricted, .denied:
            
            _ = SweetAlert().showAlert("Activa tu ubicación", subTitle: "Necesitamos tu ubicación para brindar un mejor servicio", style: AlertStyle.warning, buttonTitle:"Cancelar", buttonColor: ColorUtils.getColorUredApp(1.0), otherButtonTitle: "Ok", otherButtonColor: ColorUtils.getGrayColorApp(1.0)) { (isOtherButton) -> Void in
                if isOtherButton != true {
                    
                    if let url = URL(string:UIApplicationOpenSettingsURLString) {
                        DispatchQueue.main.async {
                            
                            if #available(iOS 10.0, *) {
                                UIApplication.shared.open(url, options: [:], completionHandler: nil)
                            } else {
                                UIApplication.shared.openURL(url)
                            }
                            
                            //UIApplication.shared.open(url, completionHandler: nil)
                            
                        }
                    }
                    
                }
            }
            
            return false
        }
        
        return true
        
    }
    

    
}
