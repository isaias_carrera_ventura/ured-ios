//
//  UserRankServiceViewController.swift
//  Ured
//
//  Created by Isaias Carrera Ventura on 28/12/17.
//  Copyright © 2017 IQBC. All rights reserved.
//

import UIKit
import Cosmos
import Kingfisher
import Alamofire
import NVActivityIndicatorView
import SwiftyButton

class UserRankServiceViewController: UIViewController, UITextViewDelegate, UITextFieldDelegate, NVActivityIndicatorViewable {

    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var categoryLabel: UILabel!
    @IBOutlet weak var rankPartner: CosmosView!
    @IBOutlet weak var textFieldComment: UITextView!
    @IBOutlet weak var rankPartnerSend: CosmosView!
    
    @IBOutlet weak var descriptionLabel: UITextView!
    
    var serviceModel : ServiceModel!
    var flagEnabled = false
    
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        textFieldComment.roundBorder()
        
        CustomBar.setNavBar((self.navigationController?.navigationBar)!, navItem: navigationItem,nav: self)
        nameLabel.text =  (serviceModel.partnerModel?.value(forKey: "name") as! String) + " " + (serviceModel.partnerModel?.value(forKey: "lastname") as! String)
        
        descriptionLabel.text = serviceModel.description
        
        categoryLabel.text = serviceModel.partnerModel?.value(forKey: "category") as? String
        rankPartner.rating = Double(self.serviceModel.partnerModel?.value(forKey: "ranking") as! Int)
    
        rankPartnerSend.didTouchCosmos = { rank in
        
            self.flagEnabled = true
            
        }
        
        if let url = URL(string: serviceModel.partnerModel?.value(forKey: "imageUrl") as! String){
            
            KingfisherManager.shared.retrieveImage(with: url, options: [.transition(.fade(1))], progressBlock: nil, completionHandler: { image, error, cacheType, imageURL in
                
                if error == nil {
                    
                    //LOAD IMAGE IN ORANGE
                    self.imageView.image = image
                    
                }
                
            })
            
        }

    
    }

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        textField.resignFirstResponder()
        return true
        
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        self.view.endEditing(true)
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func sendComments(_ sender: Any) {
        
        if flagEnabled && self.textFieldComment.text != "" {
            
            
            let headers : HTTPHeaders = ["Authorization" : "JWT \(UserController.getUserSession()!.value(forKey: "jwt") as! String)"]
            let params : Parameters = ["user":"\(UserController.getUserSession()?.value(forKey: "userId") as! String)",
                                       "partner":"\(self.serviceModel.partnerModel?.value(forKey: "partnerId") as! String)",
                                       "service":serviceModel.idService!,
                                       "ranking":Int(self.rankPartnerSend.rating),
                                       "comment":self.textFieldComment.text]
            
            self.startAnimating(Constants.getSizeForLoadingView(view: self.view), type: NVActivityIndicatorType.ballBeat, color: ColorUtils.getColorUredApp(1.0), padding: NVActivityIndicatorView.DEFAULT_PADDING)
            
            Alamofire.request(Constants.REVIEW_ENDPOINT, method: .post, parameters: params, headers: headers).validate()
            .response(completionHandler: { (dataResponse) in
                
                self.stopAnimating()
                
                if let response = dataResponse.response{
                    
                    if response.statusCode == 200 {
                        
                        _ = SweetAlert().showAlert("Comentario enviado", subTitle: "Gracias por tus comentarios", style: .success, buttonTitle: "OK", action: { (flag) in
                            
                            self.navigationController?.popToRootViewController(animated: true)
                            
                        })
                        
                    }else if response.statusCode == 422 {
                        
                        _ = SweetAlert().showAlert("Ops!", subTitle: "No pudimos enviar tu comentario. Ya calificaste el servicio.", style: AlertStyle.error, buttonTitle:"Ok", buttonColor:ColorUtils.getGreenColorApp(1.0))
                        
                    }
                    
                    else{
                        
                        _ = SweetAlert().showAlert("Ops!", subTitle: "No pudimos enviar tu comentario. Inténtalo de nuevo más tarde", style: AlertStyle.error, buttonTitle:"Ok", buttonColor:ColorUtils.getGreenColorApp(1.0))
                    }
                    
                }else{
                    
                    _ = SweetAlert().showAlert("Ops!", subTitle: "No pudimos enviar tu comentario.", style: AlertStyle.error, buttonTitle:"Ok", buttonColor:ColorUtils.getGreenColorApp(1.0))
                    
                }
                
            })
            
        }else{
            
            _ = SweetAlert().showAlert("Ops!", subTitle: "Escribe un comentario y una calificación.", style: AlertStyle.warning, buttonTitle:"Ok", buttonColor:ColorUtils.getGreenColorApp(1.0))
            
        }
        
    }

}
