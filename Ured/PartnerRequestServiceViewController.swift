//
//  PartnerRequestServiceViewController.swift
//  Ured
//
//  Created by Isaias Carrera Ventura on 01/12/17.
//  Copyright © 2017 IQBC. All rights reserved.
//

import UIKit
import Cosmos
import Alamofire
import NVActivityIndicatorView
import Kingfisher
import SwiftyButton
import DateTimePicker

class PartnerRequestServiceViewController: UIViewController, getLocationBackDelegate, NVActivityIndicatorViewable {

    @IBOutlet weak var textDescriptionJob: UITextView!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var namePartner: UILabel!
    @IBOutlet weak var categoryPartner: UILabel!
    @IBOutlet weak var rankingPartner: CosmosView!
    @IBOutlet weak var switchUrgent: UISwitch!
    @IBOutlet weak var buttonPlace: FlatButton!
    @IBOutlet weak var buttonDateTime: FlatButton!
    

    var picker: DateTimePicker?
    var location : LocationUser?
    var stringDate: String?
    var stringTime: String?
    var stringPlace: String?
    
    var partnerModel: PartnerModel!
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        CustomBar.setNavBar((self.navigationController?.navigationBar)!, navItem: navigationItem,nav: self)
        
        setupView()
        
    }

    func setupView(){
        
        textDescriptionJob.layer.borderColor = ColorUtils.getColorUredApp(1.0).cgColor
        textDescriptionJob.layer.borderWidth = 1.5
        textDescriptionJob.layer.cornerRadius = 5

        
        self.namePartner.text = (partnerModel.value(forKey: "name") as! String) + " " + (partnerModel.value(forKey: "lastname") as! String)
        self.categoryPartner.text = partnerModel.value(forKey: "category") as? String
        self.rankingPartner.rating = Double(partnerModel.value(forKey: "ranking") as! Int)
        
        if let url = URL(string: partnerModel.value(forKey: "imageUrl") as! String){
            
            KingfisherManager.shared.retrieveImage(with: url, options: [.transition(.fade(1))], progressBlock: nil, completionHandler: { image, error, cacheType, imageURL in
                
                if error == nil {
                    
                    self.imageView.image = image
                    
                }
                
            })
            
            
        }

        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func selectLocation(_ sender: Any) {
        
        if LocationController.getLocationInDisk().count > 0 {
        
            self.performSegue(withIdentifier: "selectLocationUserSegue", sender: self)

        }else{
            
            _ = SweetAlert().showAlert("Ops!", subTitle: "No tienes una ubicación guardada. Ingresa una en tu perfil.", style: AlertStyle.error, buttonTitle:"Ok", buttonColor:ColorUtils.getGreenColorApp(1.0))
        }
        
    }
    
    @IBAction func setDate(_ sender: Any) {
        
        let min = Date()
        let max = Date().addingTimeInterval(60 * 60 * 24 * 30 * 3)
        let picker = DateTimePicker.show(selected: Date(), minimumDate: min, maximumDate: max)
        picker.highlightColor = UIColor(red: 255.0/255.0, green: 138.0/255.0, blue: 138.0/255.0, alpha: 1)
        picker.darkColor = UIColor.darkGray
        picker.doneButtonTitle = "OK"
        picker.todayButtonTitle = "Hoy"
        picker.is12HourFormat = false
        picker.dateFormat = "hh:mm YYYY/MM/dd"
        //        picker.isTimePickerOnly = true
        picker.includeMonth = false // if true the month shows at top
        picker.completionHandler = { date in
            
            let formatter = DateFormatter()
            formatter.dateFormat = "HH:mm YYYY/MM/dd"
            
            let strDateTime = formatter.string(from: date)
            let strDateSplit = strDateTime.components(separatedBy: " ")
            
            self.stringTime = strDateSplit[0]
            self.stringDate = strDateSplit[1]
            
            self.buttonDateTime.setTitle(strDateTime, for: .normal)
            
        }
        self.picker = picker
        
    }
    
    
    
    func writeLocationBack(value: LocationUser) {
        
        buttonPlace.setTitle(value.value(forKey: "name") as? String, for: .normal)
        location = value
        
    }
    
    //KEYBOARD METHODS
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        textField.resignFirstResponder()
        return true
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "selectLocationUserSegue" {
            
            let vc = segue.destination as! SelectorLocationUserViewController
            vc.delegate = self
            
        }
    }
    

    @IBAction func requestForService(_ sender: Any) {
        
        if let strDate = self.stringDate , let strTime = self.stringTime , let location = self.location {
            
            if self.textDescriptionJob.text != "" {
                
                let headers : HTTPHeaders = ["Authorization" : "JWT \(UserController.getUserSession()!.value(forKey: "jwt") as! String)"]
                let params : Parameters = ["id_user":"\(UserController.getUserSession()!.value(forKey: "userId") as! String)",
                                           "description":self.textDescriptionJob.text,
                                           "address":"\(location.value(forKey: "idLocation") as! String)",
                                           "date": strDate,
                                           "hour": strTime,
                                           "express": "\(self.switchUrgent.isOn)"]
                
                self.startAnimating(Constants.getSizeForLoadingView(view: self.view), type: NVActivityIndicatorType.ballBeat, color: ColorUtils.getColorUredApp(1.0), padding: NVActivityIndicatorView.DEFAULT_PADDING)
                

                
                Alamofire.request(Constants.SERVICE_ENDPOINT + "\(partnerModel.value(forKey: "partnerId") as! String)", method: .post, parameters: params, headers: headers).validate()
                .response(completionHandler: { (dataResponse) in
                    
                    self.stopAnimating()
                    
                    if ServiceController.isRequestedCorrectly(dataResponse){
                        
                       _ = SweetAlert().showAlert("Éxito", subTitle: "Servicio agendado correctamente, en breve te mandarán la cotización del servicio requerido.", style: .success, buttonTitle: "OK", action: { (flag) in
                            
                            self.navigationController?.popToRootViewController(animated: true)
                            
                        })
                        
                    }else{
                        
                        _ = SweetAlert().showAlert("Ops!", subTitle: "Algo salió mal", style: AlertStyle.error, buttonTitle:"Ok", buttonColor:ColorUtils.getGreenColorApp(1.0))
                        
                    }
                    
                })
                
            }else{
                
                _ = SweetAlert().showAlert("Ops!", subTitle: "Agrega una descripción para más información al Socio RED", style: AlertStyle.error, buttonTitle:"Ok", buttonColor:ColorUtils.getGreenColorApp(1.0))
            }
        }else{
            
            _ = SweetAlert().showAlert("Ops!", subTitle: "Selecciona una fecha, hora y ubicación para tu servicio", style: AlertStyle.error, buttonTitle:"Ok", buttonColor:ColorUtils.getGreenColorApp(1.0))
            
        }
        
    }
    
    
}
