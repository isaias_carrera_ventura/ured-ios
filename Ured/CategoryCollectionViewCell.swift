//
//  CategoryCollectionViewCell.swift
//  Ured
//
//  Created by Isaias Carrera Ventura on 30/11/17.
//  Copyright © 2017 IQBC. All rights reserved.
//

import UIKit

class CategoryCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var iconCategory: UIImageView!
    @IBOutlet weak var nameCategory: UILabel!
    
}
