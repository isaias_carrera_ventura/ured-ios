//
//  CategoryListViewController.swift
//  Ured
//
//  Created by Isaias Carrera Ventura on 29/11/17.
//  Copyright © 2017 IQBC. All rights reserved.
//

import UIKit
import Alamofire
import NVActivityIndicatorView
import Kingfisher


class CategoryListViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource,UICollectionViewDelegateFlowLayout, NVActivityIndicatorViewable {

    @IBOutlet weak var collectionView: UICollectionView!
    fileprivate let reuseIdentifier = "CellCategory"
    var categoryToPass: Category!
    var categoryList: [Category] = [Category]()
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
    }

    override func viewDidAppear(_ animated: Bool) {
        
        CustomBar.setNavBarProfile((self.navigationController?.navigationBar)!, navItem: navigationItem,nav: self)
        
        self.startAnimating(Constants.getSizeForLoadingView(view: self.view), type: NVActivityIndicatorType.ballBeat, color: ColorUtils.getColorUredApp(1.0), padding: NVActivityIndicatorView.DEFAULT_PADDING)
        
        Alamofire.request(Constants.CATEGORY_ENDPOINT, method: .get).validate()
        .response { (dataResponse) in
            
            self.stopAnimating()
            
            let categoryListServer = CategoryController.getCategoryListFromResponse(dataResponse)
            
            if categoryListServer != nil {
                
                self.categoryList.removeAll()
                self.categoryList = categoryListServer!
                self.collectionView.reloadData()
                
            }
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
    
        self.categoryToPass = self.categoryList[indexPath.row]
        self.performSegue(withIdentifier: "showPartnerListCategorySegue", sender: self)
        
    
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "showPartnerListCategorySegue" {
            
            let vc = segue.destination as! CategoryPartnerListViewController
            vc.category = self.categoryToPass
            
        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int{
        
        return categoryList.count
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        //CellBook
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as! CategoryCollectionViewCell
        
        ImageUtils.circularImage(cell.iconCategory, color: ColorUtils.getColorUredApp(1.0).cgColor)
        cell.nameCategory.text = self.categoryList[indexPath.row].name!
        
        if let url = URL(string: categoryList[indexPath.row].icon!){
            
            KingfisherManager.shared.retrieveImage(with: url, options: [.transition(.fade(1))], progressBlock: nil, completionHandler: { image, error, cacheType, imageURL in
                
                if error == nil {
                    
                    //LOAD IMAGE IN ORANGE
                    cell.iconCategory.image = image
                    
                }
                
            })
            
            
        }

        
        return cell
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int
    {
        return 1
    }
    
    fileprivate let itemsPerRow: CGFloat = 2
    fileprivate let sectionInsets = UIEdgeInsets(top: 50.0, left: 5.0, bottom: 50.0, right: 20.0)
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
    
        let paddingSpace = sectionInsets.left * (itemsPerRow + 1)
        let availableWidth = view.frame.width - paddingSpace
        let widthPerItem = availableWidth / itemsPerRow
        return CGSize(width: widthPerItem, height: 230)

    
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsetsMake(0.0, 0.0, 0.0, 0.0)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0.0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0.0
    }
    
    
}
