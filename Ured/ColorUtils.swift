//
//  ColorUtils.swift
//  URED
//
//  Created by Isaias Carrera Ventura on 23/10/17.
//  Copyright © 2017 Isaias Carrera Ventura. All rights reserved.
//

import Foundation
import UIKit

class ColorUtils {
    
    static let yellowColor:UInt = 0xFFE600
    static let greenColor:UInt = 0x5BAB0D
    static let grayColor:UInt = 0x2E2E2E
    static let grayLightColor:UInt = 0xBDBDBD
    static let blackColor:UInt = 0x000000
    static let orangeColor:UInt = 0xe67537
    static let uredColor: UInt = 0xF40A0A
    
    static func getColorUredApp(_ alpha: Double) -> UIColor{
        return self.UIColorFromRGB(uredColor, alphaValue: alpha)

    }
    
    static func getColorOrangeApp(_ alpha: Double) -> UIColor{
        return self.UIColorFromRGB(orangeColor, alphaValue: alpha)
    }
    
    static func getGrayLightColorApp(_ alpha: Double) -> UIColor{
        return self.UIColorFromRGB(grayLightColor, alphaValue: alpha)
    }
    
    
    static func getColorYellowApp(_ alpha : Double) -> UIColor{
        return self.UIColorFromRGB(yellowColor, alphaValue: alpha)
    }
    
    static func getGreenColorApp(_ alpha : Double) -> UIColor{
        return self.UIColorFromRGB(greenColor, alphaValue: alpha)
    }
    
    static func getGrayColorApp(_ alpha: Double)-> UIColor{
        return self.UIColorFromRGB(grayColor, alphaValue: alpha)
    }
    
    static func getBlackColorApp(_ alpha: Double)-> UIColor{
        return self.UIColorFromRGB(blackColor, alphaValue: alpha)
    }
    
    
    static func UIColorFromRGB(_ rgbValue: UInt,alphaValue : Double) -> UIColor {
        
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(alphaValue)
        )
        
    }
    
    static func UIColorFromHex(_ rgbValue:UInt32)->UIColor {
        
        let red = CGFloat((rgbValue & 0xFF0000) >> 16)/256.0
        let green = CGFloat((rgbValue & 0xFF00) >> 8)/256.0
        let blue = CGFloat(rgbValue & 0xFF)/256.0
        
        return UIColor(red:red, green:green, blue:blue, alpha:1.0)
    }
    
    
}
