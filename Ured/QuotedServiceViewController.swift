//
//  QuotedServiceViewController.swift
//  Ured
//
//  Created by Isaias Carrera Ventura on 04/12/17.
//  Copyright © 2017 IQBC. All rights reserved.
//

import UIKit
import MapKit
import Cosmos
import Kingfisher
import Alamofire
import NVActivityIndicatorView
import SwiftyButton

class QuotedServiceViewController: UIViewController , MKMapViewDelegate , NVActivityIndicatorViewable {
    
    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var labelName: UILabel!
    @IBOutlet weak var labelCategory: UILabel!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var ranking: CosmosView!
    
    @IBOutlet weak var reportUser: UIButton!
    @IBOutlet weak var cancelServiceBtn: UIButton!
    
    @IBOutlet weak var textDescription: UITextView!
    @IBOutlet weak var labelDescription: UILabel!
    @IBOutlet weak var buttonDescription: FlatButton!
    
    var serviceModel : ServiceModel!
    var amountService: Double = 0
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        CustomBar.setNavBar((self.navigationController?.navigationBar)!, navItem: navigationItem,nav: self)
        
        textDescription.text = serviceModel.description!
        
        labelName.text =  (serviceModel.partnerModel?.value(forKey: "name") as! String) + " " + (serviceModel.partnerModel?.value(forKey: "lastname") as! String)
        labelCategory.text = serviceModel.partnerModel?.value(forKey: "category") as? String
        ranking.rating = Double(self.serviceModel.partnerModel?.value(forKey: "ranking") as! Int)
        
        let location = CLLocationCoordinate2D(latitude: serviceModel.location!.latitude, longitude: serviceModel.location!.longitude)
        ImageUtils.circularImage(imageView, color: ColorUtils.getColorUredApp(1.0).cgColor)
        addRadiusCircle(location: location)
        
        
        let latDelta:CLLocationDegrees = 0.03
        let lonDelta:CLLocationDegrees = 0.03
        let span = MKCoordinateSpanMake(latDelta, lonDelta)
        let locationReggion = CLLocationCoordinate2DMake(location.latitude, location.longitude)
        let region = MKCoordinateRegionMake(locationReggion, span)
        
        mapView.setRegion(region, animated: false)
        
        buttonDescription.isHidden = !serviceModel.quoted
        
        
        
        if let url = URL(string: serviceModel.partnerModel?.value(forKey: "imageUrl") as! String){
            
            KingfisherManager.shared.retrieveImage(with: url, options: [.transition(.fade(1))], progressBlock: nil, completionHandler: { image, error, cacheType, imageURL in
                
                if error == nil {
                    
                    //LOAD IMAGE IN ORANGE
                    self.imageView.image = image
                    
                }
                
            })
            
        }
        
        
        if serviceModel.quoted && !serviceModel.paid{
            let currentDate = Date()
            
            let formatter = DateFormatter()
            formatter.dateFormat = "yyyy-MM-dd"
            
            let dateService = formatter.date(from: serviceModel.date!)
            
            if dateService! < currentDate {
                
                self.reportUser.isHidden = false
                self.cancelServiceBtn.isHidden = true
                
            }else{
                
                self.cancelServiceBtn.isHidden = false
                
            }
            
        }
        
        if serviceModel.rejected{
            
            buttonDescription.isHidden = true
            cancelServiceBtn.isHidden = true
            
        } else if serviceModel.quoted && !serviceModel.paid && !serviceModel.accepted {
            
            buttonDescription.setTitle("ACEPTAR COTIZACIÓN", for: .normal)
            
        }else if serviceModel.accepted && !serviceModel.paid {
            
            buttonDescription.setTitle("PAGAR SERVICIO", for: .normal)
            
        }else{
            
            buttonDescription.setTitle("COMENTAR SERVICIO", for: .normal)
            
        }
        
        
        getServiceInformation()
        
    }
    
    func getServiceInformation(){
        
        self.startAnimating(Constants.getSizeForLoadingView(view: self.view), type: NVActivityIndicatorType.ballBeat, color: ColorUtils.getColorUredApp(1.0), padding: NVActivityIndicatorView.DEFAULT_PADDING)
        
        let headers : HTTPHeaders = ["Authorization" : "JWT \(UserController.getUserSession()!.value(forKey: "jwt") as! String)"]
        
        Alamofire.request(Constants.SERVICE_ENDPOINT + serviceModel.idService!, method: .get, headers: headers)
            .validate()
            .response { (dataResponse) in
                
                self.stopAnimating()
                
                if let price = ServiceController.getServicePrice(dataResponse){
                    
                    if price != 0 {
                        
                        self.amountService = price
                        
                        let currencyFormatter = NumberFormatter()
                        currencyFormatter.usesGroupingSeparator = true
                        currencyFormatter.numberStyle = NumberFormatter.Style.currency
                        // localize to your grouping and decimal separator
                        currencyFormatter.locale = NSLocale.current
                        let priceString = currencyFormatter.string(from: NSNumber(value: price))
                        
                        self.labelDescription.text = priceString!
                        
                    }else{
                        
                        self.labelDescription.text = "NO COTIZADO"
                        
                    }
                    
                }else{
                    
                    _ = SweetAlert().showAlert("Ops", subTitle: "Algo salió mal", style: .error, buttonTitle: "OK", action: { (flag) in
                        
                        self.navigationController?.popToRootViewController(animated: true)
                        
                    })
                    
                }
                
        }
        
        
    }
    
    func addRadiusCircle(location: CLLocationCoordinate2D){
        self.mapView.delegate = self
        let circle = MKCircle(center: location, radius: 500)
        self.mapView.add(circle)
    }
    
    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        let circle = MKCircleRenderer(overlay: overlay)
        circle.strokeColor = UIColor.red
        circle.fillColor = UIColor(red: 255, green: 0, blue: 0, alpha: 0.1)
        circle.lineWidth = 1
        return circle
        
        
    }
    
    
    @IBAction func acceptQuotation(_ sender: Any) {
        
        if serviceModel.paid {
            
            self.performSegue(withIdentifier: "rankServieSegue", sender: self)
            
        }else{
            
            
            if serviceModel.quoted && !serviceModel.paid && !serviceModel.accepted{
                
                self.acceptService()
                
            }else{
                
                self.performSegue(withIdentifier: "paymentServieSegue", sender: self)
                
            }
        }
        
    }
    
    func acceptService() {
        
        
        self.startAnimating(Constants.getSizeForLoadingView(view: self.view), type: NVActivityIndicatorType.ballBeat, color: ColorUtils.getColorUredApp(1.0), padding: NVActivityIndicatorView.DEFAULT_PADDING)
        
        let headers : HTTPHeaders = ["Authorization" : "JWT \(UserController.getUserSession()!.value(forKey: "jwt") as! String)"]
        Alamofire.request(Constants.SERVICE_ACCEPT_ENDPOINT + serviceModel.idService!, method: .put, headers: headers)
            .validate()
            .response { (dataResponse) in
                
                self.stopAnimating()
                
                if ServiceController.getServiceAcceptanceStatus(dataResponse) != nil{
                    
                    _ = SweetAlert().showAlert("Servicio aceptado", subTitle: "El socio RED llegará el día y la hora pactada.", style: .success, buttonTitle: "OK", action: { (flag) in
                        
                        self.navigationController?.popToRootViewController(animated: true)
                        
                    })
                    
                }else{
                    
                    _ = SweetAlert().showAlert("Ops", subTitle: "Algo salió mal", style: .error, buttonTitle: "OK", action: { (flag) in
                        
                        self.navigationController?.popToRootViewController(animated: true)
                        
                    })
                    
                }
                
        }
        
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "rankServieSegue" {
            
            let vc = segue.destination as! UserRankServiceViewController
            vc.serviceModel = self.serviceModel
            
        }
        
        if segue.identifier == "paymentServieSegue" {
            
            let vc = segue.destination as! PaymentViewController
            vc.serviceModel = self.serviceModel
            vc.amountService = self.amountService
            
        }
        
    }
    
    @IBAction func reportUserAction(_ sender: Any) {
        
        _ = SweetAlert().showAlert("Reportar usuario", subTitle: "¿Deseas reportar a este socio RED?se", style: .warning, buttonTitle: "Reportar", buttonColor: ColorUtils.getGrayColorApp(1.0), otherButtonTitle: "Cancelar", action: { (flag) in
            
            if flag {
                
                self.reportUserSent()
                
            }
            
        })
        
    }
    
    
    func reportUserSent(){
        
        self.startAnimating(Constants.getSizeForLoadingView(view: self.view), type: NVActivityIndicatorType.ballBeat, color: ColorUtils.getColorUredApp(1.0), padding: NVActivityIndicatorView.DEFAULT_PADDING)
        
        let headers : HTTPHeaders = ["Authorization" : "JWT \(UserController.getUserSession()!.value(forKey: "jwt") as! String)"]
        
        
        let params : Parameters = ["user":(UserController.getUserSession()?.value(forKey: "userId") as! String),
                                   "partner":serviceModel.partnerModel?.value(forKey: "partnerId") as! String,
                                   "service":serviceModel.idService!]
        
        Alamofire.request(Constants.REPORT_USER_ENDPOINT, method: .post, parameters: params, headers: headers)
            .validate()
            .response { (dataResponse) in
                
                self.stopAnimating()
                
                if ServiceController.getServiceAcceptanceStatus(dataResponse) != nil{
                    
                    _ = SweetAlert().showAlert("Usuario reportado", subTitle: "El socio RED fue reportado. Gracias por tu aportación.", style: .success, buttonTitle: "OK", action: { (flag) in
                        
                        self.navigationController?.popToRootViewController(animated: true)
                        
                    })
                    
                }else{
                    
                    _ = SweetAlert().showAlert("Ops", subTitle: "Algo salió mal", style: .error, buttonTitle: "OK", action: { (flag) in
                        
                        self.navigationController?.popToRootViewController(animated: true)
                        
                    })
                    
                }
                
        }
        
        
    }
    
    @IBAction func cancelService(_ sender: Any) {
        
        
        //1. Create the alert controller.
        let alert = UIAlertController(title: "Cancelar servicio", message: "Escribe un comentario. Tu aportación es importante.", preferredStyle: .alert)
        
        
        //2. Add the text field. You can configure it however you need.
        alert.addTextField { (textField) in
            
        }
        
        alert.addAction(UIAlertAction(title: "Salir", style: .cancel, handler: { (_) in }))
        
        // 3. Grab the value from the text field, and print it when the user clicks OK.
        alert.addAction(UIAlertAction(title: "Enviar", style: .default, handler: { [weak alert] (_) in
            
            if (alert?.textFields) != nil {
                
                let textField = alert?.textFields![0] // Force unwrapping because we know it exists.
                if let comment = textField?.text {
                    
                    if comment != "" {
                        
                        let params : Parameters = ["comment":comment]
                        let headers : HTTPHeaders = ["Authorization" : "JWT \(UserController.getUserSession()?.value(forKey: "jwt") as! String)"]
                        
                        self.startAnimating(Constants.getSizeForLoadingView(view: self.view), type: NVActivityIndicatorType.ballBeat, color: ColorUtils.getColorUredApp(1.0), padding: NVActivityIndicatorView.DEFAULT_PADDING)
                        
                        Alamofire.request(Constants.SERVICE_CANCEL_ENDPOINT + self.serviceModel.idService!, method: .put, parameters: params, headers: headers).validate()
                            .response(completionHandler: { (dataResponse) in
                                
                                self.stopAnimating()
                                
                                if dataResponse.error == nil {
                                    
                                    if dataResponse.response?.statusCode == 200 {
                                        
                                        _ = SweetAlert().showAlert("Servicio cancelado", subTitle: "Cancelaste el servicio correctamente.", style: .success, buttonTitle: "Ok", action: { (flag) in
                                            
                                            self.navigationController?.popToRootViewController(animated: true)
                                            
                                        })
                                        
                                    }else{
                                        
                                        _ = SweetAlert().showAlert("Ops!", subTitle: "Algo salió mal.", style: AlertStyle.error, buttonTitle:"Ok", buttonColor:ColorUtils.getGreenColorApp(1.0))
                                        
                                    }
                                    
                                }else{
                                    
                                    
                                    _ = SweetAlert().showAlert("Ops!", subTitle: "Algo salió mal.", style: AlertStyle.error, buttonTitle:"Ok", buttonColor:ColorUtils.getGreenColorApp(1.0))
                                    
                                }
                                
                            })
                        
                    }else{
                        
                        _ = SweetAlert().showAlert("Ops!", subTitle: "Ingresa un comentario.", style: AlertStyle.error, buttonTitle:"Ok", buttonColor:ColorUtils.getGreenColorApp(1.0))
                        
                    }
                    
                    
                }else{
                    
                    
                    _ = SweetAlert().showAlert("Ops!", subTitle: "Ingresa un comentario.", style: AlertStyle.error, buttonTitle:"Ok", buttonColor:ColorUtils.getGreenColorApp(1.0))
                    
                }
                
                
            }
            
            
        }))
        
        // 4. Present the alert.
        self.present(alert, animated: true, completion: nil)
        
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
}
