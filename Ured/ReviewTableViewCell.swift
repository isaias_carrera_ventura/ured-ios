//
//  ReviewTableViewCell.swift
//  Ured
//
//  Created by Isaias Carrera Ventura on 23/12/17.
//  Copyright © 2017 IQBC. All rights reserved.
//

import UIKit
import Cosmos

class ReviewTableViewCell: UITableViewCell {
    
    @IBOutlet weak var commentText: UITextView!
    @IBOutlet weak var rankingPartner: CosmosView!
    
    
}
