//
//  PartnerSearchTableViewCell.swift
//  Ured
//
//  Created by Isaias Carrera Ventura on 01/12/17.
//  Copyright © 2017 IQBC. All rights reserved.
//

import UIKit
import Cosmos

class PartnerSearchTableViewCell: UITableViewCell {

    @IBOutlet weak var imageViewPartner: UIImageView!
    
    @IBOutlet weak var imageViewIndicator: UIImageView!
    @IBOutlet weak var namePartner: UILabel!
    @IBOutlet weak var categoryPartner: UILabel!
    @IBOutlet weak var rankingPartner: CosmosView!

}
