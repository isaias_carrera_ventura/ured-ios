//
//  UserChangePasswordViewController.swift
//  Ured
//
//  Created by Isaias Carrera Ventura on 26/12/17.
//  Copyright © 2017 IQBC. All rights reserved.
//

import UIKit
import Alamofire
import NVActivityIndicatorView
import SwiftValidator

class UserChangePasswordViewController: UIViewController, NVActivityIndicatorViewable, ValidationDelegate {

    @IBOutlet weak var currentPassword: UITextField!
    @IBOutlet weak var newPassword: UITextField!
    @IBOutlet weak var confirmPassword: UITextField!
    
    let validator = Validator()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        currentPassword.roundBorder()
        newPassword.roundBorder()
        confirmPassword.roundBorder()
        
        validator.registerField(currentPassword, rules: [RequiredRule]())
        validator.registerField(newPassword, rules: [RequiredRule(message: "Contraseña requerida"),MinLengthRule(length: 6, message:" Mínimo 6 caractéres")])
        validator.registerField(confirmPassword, rules: [ConfirmationRule(confirmField: newPassword, message: "Las contraseñas no coinciden")])
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        CustomBar.setNavBar((self.navigationController?.navigationBar)!, navItem: navigationItem,nav: self)
        
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        textField.resignFirstResponder()
        return true
        
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        self.view.endEditing(true)
        
    }


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func changePassword(_ sender: Any) {
        
        validator.validate(self)
        
    }
    
    
    func validationSuccessful() {
        
        var headers : HTTPHeaders!
        var params: Parameters!
        var account: String = ""
        
        if RedManager.getSessionType() == Constants.userType{
       
            headers = ["Authorization" : "JWT \(UserController.getUserSession()!.value(forKey: "jwt") as! String)"]
            account = UserController.getUserSession()!.value(forKey: "account") as! String
            
        }else{
            
            headers = ["Authorization" : "JWT \(PartnerController.getPartnerSession()!.value(forKey: "jwt") as! String)"]
            account = PartnerController.getPartnerSession()!.value(forKey: "account") as! String
            
        }
       
        params = ["current":currentPassword.text!.sha1(),"new":newPassword.text!.sha1()]
        
        self.startAnimating(Constants.getSizeForLoadingView(view: self.view), type: NVActivityIndicatorType.ballBeat, color: ColorUtils.getColorUredApp(1.0), padding: NVActivityIndicatorView.DEFAULT_PADDING)
        
        
        Alamofire.request(Constants.PASSWOR_ENDPOINT_CHANGE + "\(account)/passwordchange", method: .post, parameters: params, headers: headers)
        .validate()
        .response { (dataResponse) in
        
            self.stopAnimating()
            
            if dataResponse.response?.statusCode == 200 {
                
                _ = SweetAlert().showAlert("Contraseña cambiada", subTitle: "Cambiaste tu contraseña de forma exitosa.", style: .success, buttonTitle: "Ok", action: { (flag) in
                    
                    self.navigationController?.popToRootViewController(animated: true)
                    
                })
                
            }else{
                
                
                _ = SweetAlert().showAlert("¡Algo salió mal!", subTitle: "¡Verifica que tu contraseña coincida con la actual y verifica tu conexión a internet!", style: AlertStyle.error, buttonTitle:"Ok", buttonColor:ColorUtils.getGreenColorApp(1.0))
                
            }
            
            
        }
        
    }
    
    func validationFailed(_ errors:[(Validatable ,ValidationError)]) {
        
        var errorString = ""
        
        // turn the fields to red
        for (field, error) in errors {
            if let field = field as? UITextField {
                field.layer.borderColor = UIColor.red.cgColor
                field.layer.borderWidth = 1.0
            }
            errorString = errorString + "\n\(error.errorMessage)"
            error.errorLabel?.text = error.errorMessage // works if you added labels
            error.errorLabel?.isHidden = false
        }
        
        self.view.endEditing(true)
        
        _ = SweetAlert().showAlert("Error", subTitle: "¡Ingresa los datos requeridos!\n\(errorString)", style: AlertStyle.error, buttonTitle:"Ok", buttonColor:ColorUtils.getGreenColorApp(1.0))
        
        
        
    }

    
}
