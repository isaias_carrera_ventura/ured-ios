//
//  HistorialPartnerViewController.swift
//  Ured
//
//  Created by Isaias Carrera Ventura on 29/11/17.
//  Copyright © 2017 IQBC. All rights reserved.
//

import UIKit
import Alamofire
import NVActivityIndicatorView
import Kingfisher

class HistorialPartnerViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, NVActivityIndicatorViewable{
    
    
    var serviceList : [ServiceModel] = [ServiceModel]()
    
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    override func viewDidAppear(_ animated: Bool) {
        
        CustomBar.setNavBarProfile((self.navigationController?.navigationBar)!, navItem: navigationItem,nav: self)
        
        let params : Parameters = ["status":"true"]
        let headers : HTTPHeaders = ["Authorization" : "JWT \(PartnerController.getPartnerSession()?.value(forKey: "jwt") as! String)"]
        let url = Constants.PARTNER_SERVICE_ENDPOINT + (PartnerController.getPartnerSession()?.value(forKey: "partnerId") as! String)
        
        self.startAnimating(Constants.getSizeForLoadingView(view: self.view), type: NVActivityIndicatorType.ballBeat, color: ColorUtils.getColorUredApp(1.0), padding: NVActivityIndicatorView.DEFAULT_PADDING)
        
        Alamofire.request(url, method: .post, parameters: params, headers: headers)
            .validate()
            .response { (dataResponse) in
                
                self.stopAnimating()
                
                if let list = ServiceController.getServiceListPartner(dataResponse){
                    
                    if list.count > 0{
                        
                        self.serviceList.removeAll()
                        self.serviceList.append(contentsOf: list)
                        self.tableView.reloadData()
                        
                    }else{
                        
                        _ = SweetAlert().showAlert("Ops!", subTitle: "No hay resultados", style: AlertStyle.warning, buttonTitle:"Ok", buttonColor:ColorUtils.getGreenColorApp(1.0))
                        
                    }
                    
                }else{
                    
                    _ = SweetAlert().showAlert("Ops!", subTitle: "Algo salió mal", style: AlertStyle.error, buttonTitle:"Ok", buttonColor:ColorUtils.getGreenColorApp(1.0))
                    
                }
                
        }
        
        
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        self.performSegue(withIdentifier: "showDetailServicePartnerSegue", sender: self)
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "showDetailServicePartnerSegue" {
            
            let vc = segue.destination as! ServicePartnerViewController
            vc.serviceModel = self.serviceList[self.tableView.indexPathForSelectedRow!.row]
            
        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return serviceList.count
    }
    
    // Row display. Implementers should *always* try to reuse cells by setting each cell's reuseIdentifier and querying for available reusable cells with dequeueReusableCellWithIdentifier:
    // Cell gets various attributes set automatically based on table (separators) and data source (accessory views, editing controls)
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! PartnerHistoryServiceTableViewCell
        
        cell.labelName.text = (self.serviceList[indexPath.row].userModel?.value(forKey: "name") as! String) + " " +  (self.serviceList[indexPath.row].userModel?.value(forKey: "lastname") as! String)
        cell.labelDate.text = self.serviceList[indexPath.row].date! + " " + self.serviceList[indexPath.row].hour!
        ImageUtils.circularImage(cell.profileImageview, color: ColorUtils.getColorUredApp(1.0).cgColor)
        
        if let url = URL(string: serviceList[indexPath.row].userModel?.value(forKey: "imageUrl") as! String){
            
            KingfisherManager.shared.retrieveImage(with: url, options: [.transition(.fade(1))], progressBlock: nil, completionHandler: { image, error, cacheType, imageURL in
                
                if error == nil {
                    
                    //LOAD IMAGE IN ORANGE
                    cell.profileImageview.image = image
                    
                }
                
            })
            
            
        }
        
        
        
        if serviceList[indexPath.row].rejected{
            
            cell.labelStatus.isHidden = false
            cell.labelStatus.text = "CANCELADO"
            cell.labelStatus.textColor = UIColor.red
            
        }else if serviceList[indexPath.row].quoted  {
            
            if serviceList[indexPath.row].paid {
                
                cell.labelStatus.isHidden = false
                cell.labelStatus.text = "PAGADO"
                cell.labelStatus.textColor = ColorUtils.getGreenColorApp(1.0)
                
            }else{
                
                cell.labelStatus.isHidden = false
                cell.labelStatus.text = "COTIZADO"
                cell.labelStatus.textColor = ColorUtils.getGreenColorApp(1.0)
                
            }
            
        }else{
            
            cell.labelStatus.isHidden = true
            
        }
        
        
        return cell
    }

    
    
}
