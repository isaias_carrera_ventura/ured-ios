//
//  RestorePasswordViewController.swift
//  Ured
//
//  Created by Isaias Carrera Ventura on 26/12/17.
//  Copyright © 2017 IQBC. All rights reserved.
//

import UIKit
import Alamofire
import NVActivityIndicatorView
import SwiftValidator

class RestorePasswordViewController: UIViewController, UITextFieldDelegate, ValidationDelegate, NVActivityIndicatorViewable {
    
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var navBar: UINavigationBar!
    @IBOutlet weak var navItem: UINavigationItem!
    
    let validator = Validator()

    override func viewDidLoad() {

        super.viewDidLoad()
        
        CustomBar.setNavBar((self.navBar)!, navItem: navItem,nav: self)
        
        emailTextField.roundBorder()
        validator.registerField(emailTextField, rules: [EmailRule(message:"Email requerido")])
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func restorePassword(_ sender: Any) {
    
        validator.validate(self)
    
    }
    
    
    
    func validationSuccessful() {
        
        let params : Parameters = ["email":emailTextField.text!]
        
        self.startAnimating(Constants.getSizeForLoadingView(view: self.view), type: NVActivityIndicatorType.ballBeat, color: ColorUtils.getColorUredApp(1.0), padding: NVActivityIndicatorView.DEFAULT_PADDING)
        
        Alamofire.request(Constants.PASSWOR_ENDPOINT_RESTORE , method: .post, parameters: params)
            .validate()
            .response { (dataResponse) in
                
                self.stopAnimating()
                
                if dataResponse.response?.statusCode == 200 {
                    
                    _ = SweetAlert().showAlert("Contraseña restaurada", subTitle: "Verifica tu correo electrónico, se adjunto la nueva contraseña", style: .success, buttonTitle: "Ok", action: { (flag) in
                        
                        self.dismiss(animated: true, completion: nil)
                        
                    })
                    
                }else{
                    
                    
                    _ = SweetAlert().showAlert("¡Algo salió mal!", subTitle: "¡Escribe un email registrado previamente y/o verifica tu conexión a internet!", style: AlertStyle.error, buttonTitle:"Ok", buttonColor:ColorUtils.getGreenColorApp(1.0))
                    
                }
                
        }
        
    }
    
    func validationFailed(_ errors:[(Validatable ,ValidationError)]) {
        
        var errorString = ""
        
        // turn the fields to red
        for (field, error) in errors {
            if let field = field as? UITextField {
                field.layer.borderColor = UIColor.red.cgColor
                field.layer.borderWidth = 1.0
            }
            errorString = errorString + "\n\(error.errorMessage)"
            error.errorLabel?.text = error.errorMessage // works if you added labels
            error.errorLabel?.isHidden = false
        }
        
        self.view.endEditing(true)
        
        _ = SweetAlert().showAlert("Error", subTitle: "¡Ingresa los datos requeridos!\n\(errorString)", style: AlertStyle.error, buttonTitle:"Ok", buttonColor:ColorUtils.getGreenColorApp(1.0))
        
        
        
    }

    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        textField.resignFirstResponder()
        return true
        
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        self.view.endEditing(true)
        
    }

    @IBAction func cancel(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}
