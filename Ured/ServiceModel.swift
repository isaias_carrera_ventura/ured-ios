//
//  ServiceModel.swift
//  Ured
//
//  Created by Isaias Carrera Ventura on 27/12/17.
//  Copyright © 2017 IQBC. All rights reserved.
//

import Foundation

class ServiceModel {
    
    var idService: String?
    var userModel: UserModel?
    var status: Bool = false
    var description: String?
    var location : LocationUser?
    var date,hour: String?
    var express: Bool = false
    var quoted: Bool = false
    var partnerModel: PartnerModel?
    var paid: Bool = false
    var accepted: Bool = false
    var rejected: Bool = false
    
    
}
