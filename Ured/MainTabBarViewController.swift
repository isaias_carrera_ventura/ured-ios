//
//  MainTabBarViewController.swift
//  Ured
//
//  Created by Isaias Carrera Ventura on 30/12/17.
//  Copyright © 2017 IQBC. All rights reserved.
//

import UIKit
import Alamofire
import CoreLocation

class MainTabBarViewController: UITabBarController, CLLocationManagerDelegate {
    
    var userLocation:CLLocation! = nil
    var locationManager:CLLocationManager! = nil
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        
        super.viewDidAppear(animated)
        setLocationUpdate()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    
    override func viewDidDisappear(_ animated: Bool) {
        
        self.locationManager.stopUpdatingLocation()
        
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]){
        
        self.userLocation = locations[0]
        
    }

    
    func setLocationUpdate(){
        
        //LOCATION
        locationManager = CLLocationManager()
        
        if LocationUtils.GetStatusLocation(self.locationManager, view: self) != false {
            
            locationManager.delegate = self;
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            
            locationManager.requestWhenInUseAuthorization()
            locationManager.startUpdatingLocation()
            
            self.userLocation = locationManager.location
            
            if self.userLocation != nil {
                
                requestForUpdateLocation()
            }
            
        }
        
    }
    
    func requestForUpdateLocation() {
        
        let params : Parameters = ["latitude":self.userLocation.coordinate.latitude,"longitude":self.userLocation.coordinate.longitude]
        let headers : HTTPHeaders = ["Authorization" : "JWT \(PartnerController.getPartnerSession()?.value(forKey: "jwt") as! String)"]
        let url = Constants.PARTNER_LOCATION_ENDPOINT + (PartnerController.getPartnerSession()?.value(forKey: "partnerId") as! String)
        
        Alamofire.request(url, method: .put, parameters: params, headers: headers).validate()
        .response { (dataResponse) in
            
        }
        
    }
    
}
