//
//  ReviewPartner.swift
//  Ured
//
//  Created by Isaias Carrera Ventura on 23/12/17.
//  Copyright © 2017 IQBC. All rights reserved.
//

import Foundation

class ReviewPartner {
    
    var id, user, partner, service, comment : String?
    var ranking: Int = 0
    
}
