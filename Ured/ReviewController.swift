//
//  ReviewController.swift
//  Ured
//
//  Created by Isaias Carrera Ventura on 23/12/17.
//  Copyright © 2017 IQBC. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

class ReviewController {

    class func getReviewList(_ dataResponse: DefaultDataResponse) -> [ReviewPartner]?{
        
        guard dataResponse.error == nil else{
            return nil
        }
        
        guard let data = dataResponse.data else{
            return nil
        }
        
        let json = JSON(data:data)
        let jsonPartner = json["reviews"]
        var reviewList = [ReviewPartner]()
        
        
        for (_,subJson):(String,JSON) in jsonPartner{
            
            let review = ReviewPartner()
            
            review.id = subJson["id"].stringValue
            review.user = subJson["user"].stringValue
            review.partner = subJson["partner"].stringValue
            review.service = subJson["service"].stringValue
            review.ranking = subJson["ranking"].intValue
            review.comment = subJson["comment"].stringValue
            
            reviewList.append(review)
        }
        
        return reviewList

        
    }
    
}
