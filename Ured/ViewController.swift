//
//  ViewController.swift
//  Ured
//
//  Created by Isaias Carrera Ventura on 29/11/17.
//  Copyright © 2017 IQBC. All rights reserved.
//

import UIKit
import BWWalkthrough

class ViewController: UIViewController, BWWalkthroughViewControllerDelegate {
    
    @IBOutlet weak var navItem: UINavigationItem!
    @IBOutlet weak var navBar: UINavigationBar!
    
    var loginType : Int = 0
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        CustomBar.setNavBar((self.navBar)!, navItem: navItem,nav: self)
    
    }
    
    @IBAction func prepareForUnwind(segue: UIStoryboardSegue){
        
    }

    override func viewDidAppear(_ animated: Bool) {
        
        if RedManager.isSessionInit(){
            
            let sessionType = RedManager.getSessionType()
            
            if sessionType == Constants.userType{
               
                if UserController.getUserSession() != nil{
                    
                    self.performSegue(withIdentifier: "sessionUserSegue", sender: self)
                    
                }
                
            }else{
                
                if PartnerController.getPartnerSession() != nil {
                    
                    self.performSegue(withIdentifier: "mainPartnerSegue", sender: self)
                    
                }
                
                
            }
            
        }else if !RedManager.isTutorialShown() {
            
            showWalkthrough()
            RedManager.setTutorialShown()
            
        }
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    
    @IBAction func presentUserSegue(_ sender: Any) {
    
        loginType = Constants.userType
        self.performSegue(withIdentifier: "loginSegueViewController", sender: self)
    
    }

    @IBAction func presentPartnerSegue(_ sender: Any) {
        
        loginType = Constants.partnerType
        self.performSegue(withIdentifier: "loginSegueViewController", sender: self)

    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "loginSegueViewController" {
        
            let vc = segue.destination as! LoginViewController
            vc.loginType = self.loginType
            
        }
        
    }
    
    func showWalkthrough(){
        
        // Get view controllers and build the walkthrough
        let stb = UIStoryboard(name: "Walkthrough", bundle: nil)
        let walkthrough = stb.instantiateViewController(withIdentifier: "walk") as! BWWalkthroughViewController
        let page_zero = stb.instantiateViewController(withIdentifier: "walk0")
        let page_one = stb.instantiateViewController(withIdentifier: "walk1")
        let page_two = stb.instantiateViewController(withIdentifier: "walk2")
        
        // Attach the pages to the master
        walkthrough.delegate = self
        walkthrough.add(viewController:page_zero)
        walkthrough.add(viewController:page_one)
        walkthrough.add(viewController:page_two)
        
        self.present(walkthrough, animated: true, completion: nil)
    }
    
    
    func walkthroughCloseButtonPressed() {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func goToTerms(_ sender: Any) {
        
        guard let url = URL(string: Constants.TERMS_ENDPOINT) else {
            return //be safe
        }
        
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        } else {
            UIApplication.shared.openURL(url)
        }
        
    }
    

}

