//
//  ImageUtils.swift
//  URED
//
//  Created by Isaias Carrera Ventura on 11/03/17.
//  Copyright © 2017 FullStackEngineering. All rights reserved.
//

import Foundation
import UIKit

class ImageUtils{
    
    class func getImageFromUrlInAsyncMode(_ url: String) -> Data{
        
        let url = URL(string: url)
        let data = try? Data(contentsOf: url!)
        return data!
        
    }
    
    static func circularImage(_ photoImageView: UIImageView?, color : CGColor)
    {
        photoImageView!.layer.frame = photoImageView!.layer.frame.insetBy(dx: 0, dy: 0)
        photoImageView!.layer.borderColor = color
        photoImageView!.layer.cornerRadius = photoImageView!.layer.frame.width / 2 //photoImageView!.frame.height/2
        photoImageView!.layer.masksToBounds = false
        photoImageView!.clipsToBounds = true
        photoImageView!.layer.borderWidth = 3.0
        photoImageView!.contentMode = UIViewContentMode.scaleAspectFill
        
    }
    
    class func maskRoundedImage(_ image: UIImage, radius: Float,color: CGColor,borderWidth: CGFloat) -> UIImage
    {
        let imageView: UIImageView = UIImageView(image: image)
        var layer: CALayer = CALayer()
        layer = imageView.layer
        
        layer.masksToBounds = true
        layer.cornerRadius = CGFloat(radius)
        layer.borderColor = color
        layer.borderWidth = borderWidth
        
        UIGraphicsBeginImageContext(imageView.bounds.size)
        layer.render(in: UIGraphicsGetCurrentContext()!)
        let roundedImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return roundedImage!
    }
    
    class func ScaleImage(_ image: UIImage, toSize newSize: CGSize) -> (UIImage) {
        let newRect = CGRect(x: 0,y: 0, width: newSize.width, height: newSize.height).integral
        UIGraphicsBeginImageContextWithOptions(newSize, false, 0)
        let context = UIGraphicsGetCurrentContext()
        context!.interpolationQuality = .high
        let flipVertical = CGAffineTransform(a: 1, b: 0, c: 0, d: -1, tx: 0, ty: newSize.height)
        context?.concatenate(flipVertical)
        context?.draw(image.cgImage!, in: newRect)
        let newImage = UIImage(cgImage: (context?.makeImage()!)!)
        UIGraphicsEndImageContext()
        return newImage
    }
    
    class func resizeImage(image: UIImage, targetSize: CGSize) -> UIImage {
        let size = image.size
        
        let widthRatio  = targetSize.width  / size.width
        let heightRatio = targetSize.height / size.height
        
        // Figure out what our orientation is, and use that to form the rectangle
        var newSize: CGSize
        if(widthRatio > heightRatio) {
            newSize = CGSize(width: size.width * heightRatio, height: size.height * heightRatio)
        } else {
            newSize = CGSize(width: size.width * widthRatio,  height: size.height * widthRatio)
        }
        
        // This is the rect that we've calculated out and this is what is actually used below
        let rect = CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height)
        
        // Actually do the resizing to the rect using the ImageContext stuff
        UIGraphicsBeginImageContextWithOptions(newSize, false, 1.0)
        image.draw(in: rect)
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage!
    }
    
    class func roundedRectImageFromImage(_ image:UIImage,imageSize:CGSize,cornerRadius:CGFloat,color: UIColor)->UIImage{
        
        let borderWidth: CGFloat = 2.0
        
        UIGraphicsBeginImageContextWithOptions(imageSize, false, 0)
        let bounds = CGRect(origin: CGPoint.zero, size: imageSize)
        let path = UIBezierPath(roundedRect: bounds.insetBy(dx: borderWidth / 2, dy: borderWidth / 2), cornerRadius: 10.0)
        let context = UIGraphicsGetCurrentContext()
        
        context?.saveGState()
        // Clip the drawing area to the path
        path.addClip()
        
        // Draw the image into the context
        image.draw(in: bounds)
        context?.restoreGState()
        
        // Configure the stroke
        //UIColor.grayColor().setStroke()
        color.setStroke()
        path.lineWidth = borderWidth
        
        // Stroke the border
        path.stroke()
        
        let roundedImage = UIGraphicsGetImageFromCurrentImageContext();
        
        UIGraphicsEndImageContext();
        
        return roundedImage!
        
    }
    
}
