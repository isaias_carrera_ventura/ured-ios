//
//  LocationUser.swift
//  Ured
//
//  Created by Isaias Carrera Ventura on 06/12/17.
//  Copyright © 2017 IQBC. All rights reserved.
//

import Foundation
import RealmSwift

class LocationUser: Object{
    
    dynamic var name: String?
    dynamic var address: String?
    dynamic var idLocation: String?
    dynamic var latitude: Double = 0.0
    dynamic var longitude: Double = 0.0
    
}
