//
//  SearchPartnersViewController.swift
//  Ured
//
//  Created by Isaias Carrera Ventura on 29/11/17.
//  Copyright © 2017 IQBC. All rights reserved.
//

import UIKit
import PickerView
import Alamofire
import SwiftyButton
import NVActivityIndicatorView
import Kingfisher

class SearchPartnersViewController: UIViewController , UITableViewDelegate, UITableViewDataSource ,NVActivityIndicatorViewable, getCategoryBackDelegate, getLocationBackDelegate {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var locationButton: FlatButton!
    @IBOutlet weak var categoryButton: FlatButton!
    
    var partnerList = [PartnerModel]()
    var categoryList : [Category]?
    var location: LocationUser?
    var category: Category?
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        CustomBar.setNavBarProfile((self.navigationController?.navigationBar)!, navItem: navigationItem,nav: self)
        
        if let locationOpt = location{
            
            if locationOpt.isInvalidated {
            
                self.location = nil
                self.locationButton.setTitle("UBICACIÓN", for: .normal)
                
            }
            
        }else{
            
            self.location = nil
            self.locationButton.setTitle("UBICACIÓN", for: .normal)
            
        }
        
    }
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    
        return partnerList.count
    
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        
        //CellBook
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! PartnerSearchTableViewCell
        
        cell.categoryPartner.text = partnerList[indexPath.row].value(forKey: "category") as? String
        let name = partnerList[indexPath.row].value(forKey: "name") as? String
        let lastname = partnerList[indexPath.row].value(forKey: "lastname") as? String
        cell.namePartner.text = name! + " " + lastname!
        cell.rankingPartner.rating = (partnerList[indexPath.row].value(forKey: "ranking") as? Double)!
        
        if let url = URL(string: partnerList[indexPath.row].value(forKey: "imageUrl") as! String){
            
            KingfisherManager.shared.retrieveImage(with: url, options: [.transition(.fade(1))], progressBlock: nil, completionHandler: { image, error, cacheType, imageURL in
                
                if error == nil {
                    
                    cell.imageViewPartner.image = image
                    
                }
                
            })
            
            
        }
        
        if partnerList[indexPath.row].value(forKey: "imageBusiness") as! String  != "" {
            
            if let url = URL(string: partnerList[indexPath.row].value(forKey: "imageBusiness") as! String){
                
                KingfisherManager.shared.retrieveImage(with: url, options: [.transition(.fade(1))], progressBlock: nil, completionHandler: { image, error, cacheType, imageURL in
                    
                    if error == nil {
                        
                        cell.imageViewIndicator.isHidden = false
                        cell.imageViewIndicator.image = image
                        
                    }
                    
                })
                
                
            }
            
        }else{
        
            cell.imageViewIndicator.isHidden = true
            
        }
        
        ImageUtils.circularImage(cell.imageViewPartner, color: ColorUtils.getColorUredApp(1.0).cgColor)
        ImageUtils.circularImage(cell.imageViewIndicator, color: ColorUtils.getBlackColorApp(0.5).cgColor)
        
        return cell
        
    }
    
    @IBAction func selectLocation(_ sender: Any) {
        
        if LocationController.getLocationInDisk().count > 0 {
            
            self.performSegue(withIdentifier: "showLocation", sender: self)
            
        }else{
            
            _ = SweetAlert().showAlert("Ops!", subTitle: "No tienes una ubicación guardada\nAgrega una en tu perfil", style: AlertStyle.error, buttonTitle:"Ok", buttonColor:ColorUtils.getGreenColorApp(1.0))
            
        }
        
    }
    
    @IBAction func selectCategory(_ sender: Any) {
        
        self.startAnimating(Constants.getSizeForLoadingView(view: self.view), type: NVActivityIndicatorType.ballBeat, color: ColorUtils.getColorUredApp(1.0), padding: NVActivityIndicatorView.DEFAULT_PADDING)
        
        Alamofire.request(Constants.CATEGORY_ENDPOINT, method: .get)
            .validate()
            .response { (dataResponse) in
                
                self.stopAnimating()
                
                let categoryListServer = CategoryController.getCategoryListFromResponse(dataResponse)
                
                if categoryListServer != nil {
                    
                    self.categoryList = categoryListServer!
                    self.performSegue(withIdentifier: "showCategory", sender: self)
                    
                }else{
                    
                    _ = SweetAlert().showAlert("Ops!", subTitle: "No pudimos obtener las categorías", style: AlertStyle.warning, buttonTitle:"Ok", buttonColor:ColorUtils.getGreenColorApp(1.0))
                    
                }
        }
        
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        self.performSegue(withIdentifier: "showDetailPartnerSegue", sender: self)
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "showCategory"{
            
            let vc = segue.destination as! SelectorCategoryViewController
            vc.categoryList = self.categoryList!
            vc.delegate = self
            
        }else if segue.identifier == "showLocation" {
            
            let vc = segue.destination as! SelectorLocationUserViewController
            vc.delegate = self
            
        }else if segue.identifier == "showDetailPartnerSegue" {
            
            let vc = segue.destination as! PartnerDetailViewController
            vc.partnerModel = self.partnerList[tableView.indexPathForSelectedRow!.row]
            
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func writeCategoryBack(value: Category){
        
        
        categoryButton.setTitle(value.name, for: .normal)
        category = value
        
    }
    
    
    func writeLocationBack(value: LocationUser) {
        
        locationButton.setTitle(value.value(forKey: "name") as? String, for: .normal)
        location = value
        
    }
    
    
    @IBAction func searchForPartner(_ sender: Any) {
        
        if category != nil && location != nil {
            
            let headers : HTTPHeaders = ["Authorization" : "JWT \(UserController.getUserSession()!.value(forKey: "jwt") as! String)"]
            let params : Parameters = ["category":category!.id!,"address":location?.value(forKey: "idLocation") as! String]
            
            
            self.startAnimating(Constants.getSizeForLoadingView(view: self.view), type: NVActivityIndicatorType.ballBeat, color: ColorUtils.getColorUredApp(1.0), padding: NVActivityIndicatorView.DEFAULT_PADDING)
            

            Alamofire.request(Constants.SERVICE_ENDPOINT, method: .post,parameters : params ,headers: headers).validate()
            .response(completionHandler: { (dataResponse) in
                
                self.stopAnimating()
                
                if let list = PartnerController.getListForPartners(dataResponse){
                 
                    if list.count > 0 {
                        
                        self.partnerList = list
                        self.tableView.reloadData()
                        
                    }else{
                      
                        
                        _ = SweetAlert().showAlert("Ops!", subTitle: "No hay resultados", style: AlertStyle.warning, buttonTitle:"Ok", buttonColor:ColorUtils.getGreenColorApp(1.0))
                        
                    }
                }else{
                    
                    _ = SweetAlert().showAlert("Ops!", subTitle: "Algo salió mal", style: AlertStyle.error, buttonTitle:"Ok", buttonColor:ColorUtils.getGreenColorApp(1.0))
                }
                
            })
            
        }else{
            
            _ = SweetAlert().showAlert("Ops!", subTitle: "Selecciona una ubicación y una categoría.", style: AlertStyle.warning, buttonTitle:"Ok", buttonColor:ColorUtils.getGreenColorApp(1.0))
            
        }
        
    }
    
}
