//
//  CategoryPartnerListViewController.swift
//  Ured
//
//  Created by Isaias Carrera Ventura on 23/12/17.
//  Copyright © 2017 IQBC. All rights reserved.
//

import UIKit
import Alamofire
import NVActivityIndicatorView
import Kingfisher

class CategoryPartnerListViewController: UIViewController,UITableViewDelegate, UITableViewDataSource  , NVActivityIndicatorViewable{

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var titleLabel: UILabel!
    
    var partnerList = [PartnerModel]()
    
    var category: Category?
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        CustomBar.setNavBar((self.navigationController?.navigationBar)!, navItem: navigationItem,nav: self)
        self.titleLabel.text = category!.name
        getPartnerByCategory()
        
    }
    
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return partnerList.count
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        
        //CellBook
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! PartnerCategoryTableViewCell
        
        cell.categoryPartner.text = partnerList[indexPath.row].value(forKey: "category") as? String
        let name = partnerList[indexPath.row].value(forKey: "name") as? String
        let lastname = partnerList[indexPath.row].value(forKey: "lastname") as? String
        cell.namePartner.text = name! + " " + lastname!
        cell.rankingPartner.rating = (partnerList[indexPath.row].value(forKey: "ranking") as? Double)!
        
        if let url = URL(string: partnerList[indexPath.row].value(forKey: "imageUrl") as! String){
            
            KingfisherManager.shared.retrieveImage(with: url, options: [.transition(.fade(1))], progressBlock: nil, completionHandler: { image, error, cacheType, imageURL in
                
                if error == nil {
                    
                    cell.imageViewPartner.image = image
                    
                }
                
            })
            
            
        }
        
        
        if partnerList[indexPath.row].value(forKey: "imageBusiness") as! String  != "" {
            
            if let url = URL(string: partnerList[indexPath.row].value(forKey: "imageBusiness") as! String){
                
                KingfisherManager.shared.retrieveImage(with: url, options: [.transition(.fade(1))], progressBlock: nil, completionHandler: { image, error, cacheType, imageURL in
                    
                    if error == nil {
                        
                        cell.imageIndicator.isHidden = false
                        cell.imageIndicator.image = image
                        
                    }
                    
                })
                
                
            }
            
        }else{
            
            cell.imageIndicator.isHidden = true
            
        }
        
        
        ImageUtils.circularImage(cell.imageIndicator, color: ColorUtils.getBlackColorApp(0.5).cgColor)

        
        ImageUtils.circularImage(cell.imageViewPartner, color: ColorUtils.getColorUredApp(1.0).cgColor)
        
        return cell
        
    }

    
    func getPartnerByCategory() {
        
        self.startAnimating(Constants.getSizeForLoadingView(view: self.view), type: NVActivityIndicatorType.ballBeat, color: ColorUtils.getColorUredApp(1.0), padding: NVActivityIndicatorView.DEFAULT_PADDING)
        
        let headers : HTTPHeaders = ["Authorization" : "JWT \(UserController.getUserSession()!.value(forKey: "jwt") as! String)"]
        
        Alamofire.request(Constants.CATEGORY_ENDPOINT + category!.id!, method: .get, headers: headers)
            .validate()
            .response { (dataResponse) in
                
                self.stopAnimating()
                
                if let partnerByCategoryList = CategoryController.getParterByCategoryListFromResponse(dataResponse) {
                    
                    if partnerByCategoryList.count > 0{
                        
                        self.partnerList.removeAll()
                        self.partnerList.append(contentsOf: partnerByCategoryList)
                        self.tableView.reloadData()
                        
                    }else{
                        
                        _ = SweetAlert().showAlert("Ops!", subTitle: "No hay resultados", style: AlertStyle.warning, buttonTitle:"Ok", buttonColor:ColorUtils.getGreenColorApp(1.0))
                    }
                    
                }else{
                    
                    
                    _ = SweetAlert().showAlert("Ops!", subTitle: "Algo salió mal", style: AlertStyle.error, buttonTitle:"Ok", buttonColor:ColorUtils.getGreenColorApp(1.0))
                }
                
                
                
        }

        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        self.performSegue(withIdentifier: "showDetailPartnerSegue", sender: self)
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "showDetailPartnerSegue" {
            
            let vc = segue.destination as! PartnerDetailViewController
            vc.partnerModel = self.partnerList[tableView.indexPathForSelectedRow!.row]
            
        }
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    


}
