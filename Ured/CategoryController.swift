//
//  CategoryController.swift
//  Ured
//
//  Created by Isaias Carrera Ventura on 30/11/17.
//  Copyright © 2017 IQBC. All rights reserved.
//

import Foundation
import SwiftyJSON
import Alamofire

class CategoryController {

    class func getCategoryListFromResponse(_ dataResponse: DefaultDataResponse) -> [Category]? {
        
        guard dataResponse.error == nil else{
            return nil
        }
        
        guard let data = dataResponse.data else{
            return nil
        }
        
        
        let json = JSON(data:data)
        let jsonCategories = json["categories"]
        
        var categoryArray = [Category]()
        
        for (_,subJson):(String,JSON) in jsonCategories{
        
            let name = subJson["name"].stringValue
            let icon = subJson["icon"].stringValue
            let id = subJson["id"].stringValue
            
            categoryArray.append(Category(id: id, icon: icon, name: name))
            
        }
        
        return categoryArray
        
    }
    
    class func getParterByCategoryListFromResponse(_ dataResponse: DefaultDataResponse) -> [PartnerModel]? {
    
        
        guard dataResponse.error == nil else{
            return nil
        }
        
        guard let data = dataResponse.data else{
            return nil
        }
        
        let json = JSON(data:data)
        let jsonPartner = json["partners"]
        var partnerList = [PartnerModel]()
        
        for (_,subJson):(String,JSON) in jsonPartner{
            
            let account = subJson["account"].stringValue
            let partnerId = subJson["partnerId"].stringValue
            let rfc = subJson["rfc"].stringValue
            let name = subJson["name"].stringValue
            let lastname = subJson["lastname"].stringValue
            let email = subJson["email"].stringValue
            let category = subJson["category"].stringValue
            let imageUrl = subJson["profile_image"].stringValue
            let ranking = subJson["ranking"].intValue
            let latitude = subJson["latitude"].doubleValue
            let longitude = subJson["longitude"].doubleValue
            
            let isBusiness = subJson["isBusiness"].boolValue

            
            let partner = PartnerModel()
            partner.account = account
            partner.partnerId = partnerId
            partner.rfc = rfc
            partner.name = name
            partner.lastname = lastname
            partner.email = email
            partner.category = category
            partner.imageUrl = imageUrl
            partner.ranking = ranking
            partner.latitude = latitude
            partner.longitude = longitude
            
            let idBusiness = subJson["myPartner"].stringValue
            
            if !isBusiness && idBusiness != "" {
                
                let jsonArray = subJson["myPartner_info"]
                
                let jsonObject = jsonArray[0]["profile_image"].stringValue
                
                partner.isBusiness = isBusiness ? true : false
                partner.idBusiness = idBusiness
                partner.imageBusiness = jsonObject
                
                
            }else{
                
                partner.isBusiness = true
                partner.imageBusiness = ""
                partner.idBusiness = ""
                
            }

            
            partnerList.append(partner)
            
        }
        
        return partnerList
        
    }
    
    

}
