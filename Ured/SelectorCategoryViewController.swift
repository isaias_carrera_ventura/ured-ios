//
//  SelectorCategoryViewController.swift
//  
//
//  Created by Isaias Carrera Ventura on 01/12/17.
//
//

import UIKit
import PickerView
import Alamofire

class SelectorCategoryViewController: UIViewController, PickerViewDataSource, PickerViewDelegate {

    @IBOutlet weak var pickerView: PickerView!
    var categoryList : [Category]!
    var delegate: getCategoryBackDelegate?
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        pickerView.selectionStyle = PickerView.SelectionStyle.overlay
        
        self.pickerView.delegate = self
        self.pickerView.dataSource = self
       
        if self.navigationController != nil {
            
            CustomBar.setNavBar((self.navigationController?.navigationBar)!, navItem: navigationItem,nav: self)
        }

               
    }    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    func pickerViewNumberOfRows(_ pickerView: PickerView) -> Int {
        
        return self.categoryList.count
    }
    
    func pickerView(_ pickerView: PickerView, titleForRow row: Int, index: Int) -> String {
        
        let item = self.categoryList[index].name!
        return item
        
    }
    
    func pickerViewHeightForRows(_ pickerView: PickerView) -> CGFloat {
        return 70.0 // In this example I'm returning arbitrary 50.0pt but you should return the row height you want.
    }
    
    func pickerView(_ pickerView: PickerView, styleForLabel label: UILabel, highlighted: Bool) {
        label.textAlignment = .center
        
        if highlighted {
            label.font = UIFont.systemFont(ofSize: 17.0)
            label.textColor = view.tintColor
        } else {
            label.font = UIFont.systemFont(ofSize: 13.0)
            label.textColor = .lightGray
        }
    }
    
    func pickerView(_ pickerView: PickerView, didTapRow row: Int, index: Int) {
    
        delegate?.writeCategoryBack(value: categoryList[index])
        
        if self.navigationController != nil {
        
            navigationController?.popViewController(animated: true)
            
        }else{
            
            dismiss(animated: true, completion: nil)
            
        }
        
        
        
        
    }
}
