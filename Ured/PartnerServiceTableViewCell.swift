//
//  PartnerServiceTableViewCell.swift
//  Ured
//
//  Created by Isaias Carrera Ventura on 30/12/17.
//  Copyright © 2017 IQBC. All rights reserved.
//

import UIKit

class PartnerServiceTableViewCell: UITableViewCell {

    @IBOutlet weak var labelName: UILabel!
    @IBOutlet weak var labelDate: UILabel!
    @IBOutlet weak var labelStatus: UILabel!
    @IBOutlet weak var profileImageview: UIImageView!
    
}
