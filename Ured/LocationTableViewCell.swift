//
//  LocationTableViewCell.swift
//  Ured
//
//  Created by Isaias Carrera Ventura on 24/12/17.
//  Copyright © 2017 IQBC. All rights reserved.
//

import UIKit

class LocationTableViewCell: UITableViewCell {

    
    @IBOutlet weak var locationName: UILabel!
    @IBOutlet weak var locationDescription: UILabel!
    
}
