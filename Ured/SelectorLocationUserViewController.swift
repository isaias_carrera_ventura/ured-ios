//
//  SelectorLocationUserViewController.swift
//  Ured
//
//  Created by Isaias Carrera Ventura on 01/12/17.
//  Copyright © 2017 IQBC. All rights reserved.
//

import UIKit
import PickerView

class SelectorLocationUserViewController: UIViewController, PickerViewDataSource, PickerViewDelegate {

    @IBOutlet weak var pickerView: PickerView!
    var delegate: getLocationBackDelegate?
    var locationUser = [LocationUser]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        CustomBar.setNavBar((self.navigationController?.navigationBar)!, navItem: navigationItem,nav: self)
        pickerView.selectionStyle = PickerView.SelectionStyle.overlay
        pickerView.delegate = self
        pickerView.dataSource = self
        
        locationUser = LocationController.getLocationInDisk()
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func pickerViewNumberOfRows(_ pickerView: PickerView) -> Int {
        return locationUser.count
    }
    
    func pickerView(_ pickerView: PickerView, titleForRow row: Int, index: Int) -> String {
        let item = locationUser[index].value(forKey: "name") as! String
        return item
    }
    
    func pickerViewHeightForRows(_ pickerView: PickerView) -> CGFloat {
        return 70.0 // In this example I'm returning arbitrary 50.0pt but you should return the row height you want.
    }
    
    func pickerView(_ pickerView: PickerView, styleForLabel label: UILabel, highlighted: Bool) {
        label.textAlignment = .center
        
        if highlighted {
            label.font = UIFont.systemFont(ofSize: 17.0)
            label.textColor = view.tintColor
        } else {
            label.font = UIFont.systemFont(ofSize: 13.0)
            label.textColor = .lightGray
        }
    }
    
    func pickerView(_ pickerView: PickerView, didTapRow row: Int, index: Int) {
        
        delegate?.writeLocationBack(value: locationUser[index])
        navigationController?.popViewController(animated: true)
        
    }

}
