
//
//  ActiveServiceUserViewController.swift
//  Ured
//
//  Created by Isaias Carrera Ventura on 30/11/17.
//  Copyright © 2017 IQBC. All rights reserved.
//

import UIKit
import Alamofire
import Kingfisher
import NVActivityIndicatorView



class ServiceUserListViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, NVActivityIndicatorViewable {

    
    @IBOutlet weak var tableView: UITableView!
    
    var serviceArray = [ServiceModel]()
    
    var status : Bool = false
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        self.startAnimating(Constants.getSizeForLoadingView(view: self.view), type: NVActivityIndicatorType.ballBeat, color: ColorUtils.getColorUredApp(1.0), padding: NVActivityIndicatorView.DEFAULT_PADDING)
        
        
        let headers : HTTPHeaders = ["Authorization" : "JWT \(UserController.getUserSession()!.value(forKey: "jwt") as! String)"]
        Alamofire.request(Constants.SERVICE_USER_ENDPOINT + (UserController.getUserSession()?.value(forKey: "userId") as! String)
            , method: .get, headers: headers)
        .validate()
        .response { (dataResponse) in
            
            self.stopAnimating()
            
            if let list = ServiceController.getServiceListUserByStatus(dataResponse, self.status){
                
                if list.count <= 0 {
                    
                    _ = SweetAlert().showAlert("Ops!", subTitle: "No hay resultados", style: AlertStyle.warning, buttonTitle:"Ok", buttonColor:ColorUtils.getGreenColorApp(1.0))
                    
                }else{
                
                    self.serviceArray.removeAll()
                    self.serviceArray.append(contentsOf: list)
                    self.tableView.reloadData()
                    
                }
                
            }else{
                
                _ = SweetAlert().showAlert("Ops!", subTitle: "Algo salió mal", style: AlertStyle.error, buttonTitle:"Ok", buttonColor:ColorUtils.getGreenColorApp(1.0))
            }
            
        }
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return serviceArray.count
        
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        
        //CellBook
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! UserServiceTableViewCell
        
        cell.dateService.text = self.serviceArray[indexPath.row].date! + " " + self.serviceArray[indexPath.row].hour!
        cell.nameService.text = (self.serviceArray[indexPath.row].partnerModel?.value(forKey: "name") as! String) + " " + (self.serviceArray[indexPath.row].partnerModel?.value(forKey: "lastname") as! String)
        cell.ranking.rating = Double(self.serviceArray[indexPath.row].partnerModel?.value(forKey: "ranking") as! Int)
        ImageUtils.circularImage(cell.imageViewPartnerService, color: ColorUtils.getColorUredApp(1.0).cgColor)
        
        if let url = URL(string: serviceArray[indexPath.row].partnerModel?.value(forKey: "imageUrl") as! String){
            
            KingfisherManager.shared.retrieveImage(with: url, options: [.transition(.fade(1))], progressBlock: nil, completionHandler: { image, error, cacheType, imageURL in
                
                if error == nil {
                    
                    //LOAD IMAGE IN ORANGE
                    cell.imageViewPartnerService.image = image
                    
                }
                
            })
            
            
        }
        if serviceArray[indexPath.row].rejected{
            
            cell.isPaid.isHidden = false
            cell.isPaid.text = "CANCELADO"
            cell.isPaid.textColor = UIColor.red
            
        }else if serviceArray[indexPath.row].quoted  {
            
            if serviceArray[indexPath.row].paid {
            
                cell.isPaid.isHidden = false
                cell.isPaid.text = "PAGADO"
                cell.isPaid.textColor = ColorUtils.getGreenColorApp(1.0)
                
            }else{
                
                cell.isPaid.isHidden = false
                cell.isPaid.text = "COTIZADO"
                cell.isPaid.textColor = ColorUtils.getGreenColorApp(1.0)
                
            }
            
        }else{
            
            cell.isPaid.isHidden = true
            
        }
        
        return cell
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "showServiceQuoted" {
            
            let vc = segue.destination as! QuotedServiceViewController
            vc.serviceModel = self.serviceArray[tableView.indexPathForSelectedRow!.row]
            
        }
        
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        self.performSegue(withIdentifier: "showServiceQuoted", sender: self)
        
    }

}
