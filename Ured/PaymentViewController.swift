//
//  PaymentViewController.swift
//  Ured
//
//  Created by Isaias Carrera Ventura on 29/12/17.
//  Copyright © 2017 IQBC. All rights reserved.
//

import UIKit
import Alamofire
import NVActivityIndicatorView
import SwiftValidator

class PaymentViewController: UIViewController , UITextFieldDelegate, ValidationDelegate, NVActivityIndicatorViewable{
    
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var noTarjetTextField: UITextField!
    @IBOutlet weak var cvvTextField: UITextField!
    @IBOutlet weak var yearTextField: UITextField!
    @IBOutlet weak var monthTextField: UITextField!
    
    var serviceModel : ServiceModel!
    let conekta = Conekta()
    let validator = Validator()
    var amountService: Double = 0
    
    override func viewDidLoad() {
        
        setupView()

        super.viewDidLoad()
        
    }
    
    func setupView(){

        CustomBar.setNavBar((self.navigationController?.navigationBar)!, navItem: navigationItem,nav: self)

        conekta.delegate = self
        conekta.publicKey = "key_RiGEdEafrvBrLytasqWnsAFg"
        
        conekta.collectDevice()
        
        
        nameTextField.roundBorder()
        noTarjetTextField.roundBorder()
        cvvTextField.roundBorder()
        yearTextField.roundBorder()
        monthTextField.roundBorder()
        
        
        validator.registerField(nameTextField, rules: [RequiredRule(message: "Escribe el nombre del titular")])
        validator.registerField(noTarjetTextField, rules: [RequiredRule(message: "Escribe el número de tarjeta"),
                                                           MinLengthRule(length: 16, message: "Escribe el número de tarjeta."),
                                                           MaxLengthRule(length: 18, message: "Escribe el número de tarjeta.")])
        validator.registerField(cvvTextField, rules: [RequiredRule(message: "Escribe el cvv de tarjeta"),
                                                      MinLengthRule(length: 3, message: "Escribe el cvv de tarjeta."),
                                                      MaxLengthRule(length: 4, message: "Escribe el cvv de tarjeta.")])
        validator.registerField(yearTextField, rules: [RegexRule(regex: "[0-9]{4}", message: "Escribe el año")])
        validator.registerField(monthTextField, rules: [RegexRule(regex: "[0-9]{2}", message: "Escribe el mes")])
        
    }
    
    @IBAction func payMethod(_ sender: Any) {
        
        validator.validate(self)
        
    }
    
    func validationSuccessful() {
        
        self.view.endEditing(true)
        
        let card = conekta.card()
        
        card?.setNumber(noTarjetTextField.text!, name: nameTextField.text!, cvc: cvvTextField.text!, expMonth: monthTextField.text!, expYear: yearTextField.text!)
        
        let token = conekta.token()
        
        token?.card = card
        
        self.startAnimating(Constants.getSizeForLoadingView(view: self.view), type: NVActivityIndicatorType.ballBeat, color: ColorUtils.getColorUredApp(1.0), padding: NVActivityIndicatorView.DEFAULT_PADDING)
        
        token?.create(success: { (data) -> Void in
            
            self.stopAnimating()
            if let strId = data?["id"] as? String{
                
                self.doPaymentTransaction(cardId: strId)
                
            }else if let str = data?["message_to_purchaser"] as? String {
                
                _ = SweetAlert().showAlert("Error", subTitle: "\(str)", style: AlertStyle.error, buttonTitle:"Ok", buttonColor:ColorUtils.getGreenColorApp(1.0))
                
            }else{
                
                _ = SweetAlert().showAlert("Error", subTitle: "Algo saló mal", style: AlertStyle.error, buttonTitle:"Ok", buttonColor:ColorUtils.getGreenColorApp(1.0))
                
            }
            
            
        }, andError: { (error) -> Void in
            
            self.stopAnimating()
            _ = SweetAlert().showAlert("Error", subTitle: "¡No nos pudimos comunicar con el servidor!", style: AlertStyle.error, buttonTitle:"Ok", buttonColor:ColorUtils.getGreenColorApp(1.0))
            
        })
        
        
    }
    
    func doPaymentTransaction(cardId: String){
        
        let headers : HTTPHeaders = ["Authorization" : "JWT \(UserController.getUserSession()!.value(forKey: "jwt") as! String)"]
        let params : Parameters = ["card":cardId,"amount":self.amountService,"service":self.serviceModel.idService!]
        
        
        self.startAnimating(Constants.getSizeForLoadingView(view: self.view), type: NVActivityIndicatorType.ballBeat, color: ColorUtils.getColorUredApp(1.0), padding: NVActivityIndicatorView.DEFAULT_PADDING)
        
        Alamofire.request(Constants.PAYMENT_ENDPOINT + (UserController.getUserSession()?.value(forKey: "userId") as! String), method: .post,parameters : params ,headers: headers).validate()
            .response(completionHandler: { (dataResponse) in
                
                self.stopAnimating()
                
                if ServiceController.getServiceAcceptanceStatus(dataResponse) != nil {
                    
                    let headers : HTTPHeaders = ["Authorization":"JWT \(UserController.getUserSession()?.value(forKey: "jwt") as! String)"]
                    Alamofire.request("\(Constants.SERVICE_ENDPOINT)\(self.serviceModel.idService!)", method: .put, parameters: params, headers: headers).validate()
                        .response(completionHandler: { (dataResponse) in
                          
                            
                        })
                    
                    _ = SweetAlert().showAlert("Pago realizado", subTitle: "Gracias por contratar los servicios a través de URED.", style: .success, buttonTitle: "Ok", action: { (flag) in
                        
                        self.navigationController?.popToRootViewController(animated: true)
                        
                    })
                    
                }else{
                    
                    _ = SweetAlert().showAlert("Error", subTitle: "¡Algo salió mal!", style: AlertStyle.error, buttonTitle:"Ok", buttonColor:ColorUtils.getGreenColorApp(1.0))
                    
                }
                
            })
        
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        textField.resignFirstResponder()
        return true
        
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        self.view.endEditing(true)
        
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        if textField == yearTextField || textField == monthTextField || cvvTextField == textField{
            
            scrollView.setContentOffset(CGPoint(x:0,y:150), animated: true)
        }
        
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        scrollView.setContentOffset(CGPoint(x:0,y:0), animated: true)
        
    }
    
    
    func validationFailed(_ errors:[(Validatable ,ValidationError)]) {
        
        var errorString = ""
        
        // turn the fields to red
        for (field, error) in errors {
            if let field = field as? UITextField {
                field.layer.borderColor = UIColor.red.cgColor
                field.layer.borderWidth = 1.0
            }
            errorString = errorString + "\n\(error.errorMessage)"
            error.errorLabel?.text = error.errorMessage // works if you added labels
            error.errorLabel?.isHidden = false
        }
        
        self.view.endEditing(true)
        
        _ = SweetAlert().showAlert("Error", subTitle: "¡Ingresa los datos requeridos!\n\(errorString)", style: AlertStyle.error, buttonTitle:"Ok", buttonColor:ColorUtils.getGreenColorApp(1.0))
        
        
        
    }
    
}
