//
//  ProfileEditUserViewController.swift
//  Ured
//
//  Created by Isaias Carrera Ventura on 19/02/18.
//  Copyright © 2018 IQBC. All rights reserved.
//

import Foundation
import UIKit
import NVActivityIndicatorView
import Alamofire
import SwiftValidator
import AVFoundation
import Photos
import CoreData
import Kingfisher

class ProfileEditUserViewController: UIViewController, UITextFieldDelegate, ValidationDelegate, NVActivityIndicatorViewable,UINavigationControllerDelegate, UIImagePickerControllerDelegate   {
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var lastnameEditText: UITextField!
    
    var isImageLoaded = false
    
    let validator = Validator()
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        nameTextField.roundBorder()
        lastnameEditText.roundBorder()
        
        validator.registerField(nameTextField, rules:[RequiredRule(message: "Parámetro requerido")])
        validator.registerField(lastnameEditText, rules:[RequiredRule(message: "Parámetro requerido")])
        
        nameTextField.text = (UserController.getUserSession()?.value(forKey: "name") as! String)
        lastnameEditText.text = (UserController.getUserSession()?.value(forKey: "lastname") as! String)
        
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(imageTapped(tapGestureRecognizer:)))
        imageView.isUserInteractionEnabled = true
        imageView.addGestureRecognizer(tapGestureRecognizer)
        
    }
    


    func imageTapped(tapGestureRecognizer: UITapGestureRecognizer)
    {
        
        let alertController = UIAlertController(
            title: "Edita tu foto de perfil",
            message: "Selecciona un método para tomar la foto",
            preferredStyle: .actionSheet)
        
        let galleryPreference = UIAlertAction(title: "Galería", style: .default) { (action) in
            
            self.galleryActionConfiguration()
            
        }
        
        alertController.addAction(galleryPreference)
        
        let cameraPreference = UIAlertAction(title: "Cámara", style: .default) { (action) in
            
            self.cameraActionConfiguration()
            
        }
        
        alertController.addAction(cameraPreference)
        
        let cancelAction = UIAlertAction(title: "Cancelar", style: .cancel) { (action) in
            
            alertController.dismiss(animated: true, completion: nil)
            
        }
        
        alertController.addAction(cancelAction)
        
        self.present(alertController, animated: true, completion: nil)
        
    }
    
    func cameraActionConfiguration() -> Void{
        
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera){
            
            AVCaptureDevice.requestAccess(forMediaType: AVMediaTypeVideo, completionHandler: { (Bool) in
                
                if Bool{
                    
                    self.selectCameraImage()
                    
                }else{
                    
                    DispatchQueue.main.async {
                        
                        _ = SweetAlert().showAlert("Acceso a cámara", subTitle: "Permite el acceso a la cámara para poder tomar tu foto.", style: AlertStyle.error)
                        
                        
                    }
                }
                
            })
            
        }else{
            
            
            _ = SweetAlert().showAlert("Ooops", subTitle: "Tu dispositivo no cuenta con una cámara", style: AlertStyle.error)
            
        }
        
        
    }
    
    func galleryActionConfiguration() -> Void{
        
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.photoLibrary) && (UIImagePickerController.availableMediaTypes(for: UIImagePickerControllerSourceType.photoLibrary) != nil){
            
            PHPhotoLibrary.requestAuthorization({ (PHAuthorizationStatus) in
                
                if PHPhotoLibrary.authorizationStatus() == .authorized{
                    
                    self.selectGalleryImage()
                    
                }else{
                    
                    DispatchQueue.main.async {
                        
                        _ = SweetAlert().showAlert("Acceso a galería", subTitle: "Permite el acceso a tus fotos para poder guardar una de perfil.", style: AlertStyle.warning)
                        
                    }
                    
                }
            })
        }
        else{
            
            _ = SweetAlert().showAlert("Ooops", subTitle: "Tu dispositivo no puede abrir la galería.", style: AlertStyle.error)
            
        }
        
    }
    
    func selectGalleryImage(){
        
        let imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        imagePicker.mediaTypes = ["public.image"]
        imagePicker.sourceType = UIImagePickerControllerSourceType.photoLibrary;
        imagePicker.modalPresentationStyle = .popover
        
        DispatchQueue.global().async {
            DispatchQueue.main.async {
                self.present(imagePicker, animated: true, completion: nil)
            }
        }
        
    }
    
    func selectCameraImage(){
        
        let imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        imagePicker.sourceType = UIImagePickerControllerSourceType.camera;
        imagePicker.mediaTypes = ["public.image"]
        imagePicker.modalPresentationStyle = .popover
        
        DispatchQueue.global().async {
            DispatchQueue.main.async {
                
                self.present(imagePicker, animated: true, completion: nil)
                
            }
        }
        
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            
            self.imageView.image = pickedImage
            self.isImageLoaded = true
            ImageUtils.circularImage(self.imageView, color: ColorUtils.getColorUredApp(1.0).cgColor)
            
        }
        
        self.dismiss(animated: true, completion: nil)
        
        
    }
    
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        
        CustomBar.setNavBar((self.navigationController?.navigationBar)!, navItem: navigationItem,nav: self)
        
    }
    
    //KEYBOARD METHODS
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        self.view.endEditing(true)
        
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        textField.resignFirstResponder()
        return true
        
    }
    
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        if textField == self.lastnameEditText {
         
            scrollView.setContentOffset(CGPoint(x: 0, y: 100), animated: true)
            
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        scrollView.setContentOffset(CGPoint(x: 0, y: 0), animated: true)
        
    }
    @IBAction func saveChanges(_ sender: Any) {
        
        validator.validate(self)
        
    }
    
    func validationSuccessful() {
        
        self.startAnimating(Constants.getSizeForLoadingView(view: self.view), type: NVActivityIndicatorType.ballBeat, color: ColorUtils.getColorUredApp(1.0), padding: NVActivityIndicatorView.DEFAULT_PADDING)
        
        
        let imgData = UIImageJPEGRepresentation(self.imageView.image!, 0.2)!
        let name = self.nameTextField.text!.data(using: String.Encoding.utf8, allowLossyConversion: false)!
        let lastname = self.lastnameEditText.text!.data(using: String.Encoding.utf8, allowLossyConversion: false)!
        
        
        let url = "\(Constants.USER_ENDPOINT)\(UserController.getUserSession()?.value(forKey: "userId") as! String)"
        
        Alamofire.upload(multipartFormData: { multipartFormData in
            
            if self.isImageLoaded {
                
                multipartFormData.append(imgData, withName: "profile_image",fileName: "profile_picture.jpg", mimeType: "image/jpg")
            }
            
            multipartFormData.append(name, withName: "name")
            multipartFormData.append(lastname, withName: "lastname")
            
            
        },to: url, method: .put)
        { (result) in
            switch result {
            case .success(let upload, _, _):
                
                upload.responseJSON { response in
                    
                    self.stopAnimating()
                    
                    self.isImageLoaded = false
                    
                    guard response.error == nil else{
                        
                        _ = SweetAlert().showAlert("Ooops", subTitle: "¡Algo salió mal!", style: AlertStyle.error)
                        return
                        
                    }
                    
                    guard let dataResponse = response.data else{
                        
                        _ = SweetAlert().showAlert("Ooops", subTitle: "¡Algo salió mal!", style: AlertStyle.error)
                        return
                        
                    }
                    
                    let jsonResponse = try! JSONSerialization.jsonObject(with: dataResponse, options: .allowFragments)
                    let jsonResponseChangeImage = jsonResponse as! [String:Any]
                    let image = jsonResponseChangeImage["profile_image"] as! String
                    let name = jsonResponseChangeImage["name"] as! String
                    let lastname = jsonResponseChangeImage["lastname"] as! String
                    
                    UserController.updateSession(image,name,lastname)
                    
                    _ = SweetAlert().showAlert("Cambios guardados", subTitle: "¡Perfil actualizado!", style: AlertStyle.success, buttonTitle:"Ok", buttonColor:ColorUtils.getColorUredApp(1.0))
                    
                    
                    
                }
                
            case .failure( _ ):
                self.stopAnimating()
                
                _ = SweetAlert().showAlert("Ooops", subTitle: "¡Algo salió mal!", style: AlertStyle.error)
            }
        }
        
        
    }
    
    
    func validationFailed(_ errors:[(Validatable ,ValidationError)]) {
        
        var stringError = ""
        
        for (field, error) in errors {
            if let field = field as? UITextField {
                field.layer.borderColor = UIColor.red.cgColor
                field.layer.borderWidth = 1.0
            }
            
            stringError = stringError + "\n\(error.errorMessage)"
            error.errorLabel?.text = error.errorMessage // works if you added labels
            error.errorLabel?.isHidden = false
            
        }
        
        
        self.view.endEditing(true)
        _ = SweetAlert().showAlert("Error", subTitle: "¡Ingresa los datos requeridos! \(stringError)", style: AlertStyle.error, buttonTitle:"Ok", buttonColor:ColorUtils.getColorUredApp(1.0))
        
        
    }
    
}
