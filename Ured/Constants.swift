//
//  Constants.swift
//  Ured
//
//  Created by Isaias Carrera Ventura on 29/11/17.
//  Copyright © 2017 IQBC. All rights reserved.
//

import Foundation
import UIKit

class Constants {

    static let userType = 1
    static let partnerType = 2
    
    static let URL_SERVER = "https://www.ured.com.mx:4511" //"http://34.213.221.198:4510"
    static let SERVICE_CANCEL_ENDPOINT = "\(URL_SERVER)/service/reject/"
    static let CATEGORY_ENDPOINT = "\(URL_SERVER)/category/"
    static let LOGIN_ENDPOINT = "\(URL_SERVER)/accounts/login/"
    static let SIGNUP_ENDPOINT = "\(URL_SERVER)/accounts/"
    static let USER_ENDPOINT = "\(URL_SERVER)/user/"
    static let PARTNER_ENDPOINT = "\(URL_SERVER)/partner/"
    static let ADDRESS_ENDPOINT = "\(URL_SERVER)/address/"
    static let SERVICE_ENDPOINT = "\(URL_SERVER)/service/"
    static let SERVICE_USER_ENDPOINT = "\(URL_SERVER)/user/service/"
    static let REVIEW_ENDPOINT = "\(URL_SERVER)/review/"
    static let PASSWOR_ENDPOINT_CHANGE = "\(URL_SERVER)/accounts/"
    static let PASSWOR_ENDPOINT_RESTORE = "\(URL_SERVER)/accounts/passwordrestore"
    static let TERMS_ENDPOINT = "\(URL_SERVER)/pdf/RED.pdf"
    static let ACTIVE_SERVICE = false
    static let INACTIVE_SERVICE = true
    static let SERVICE_ACCEPT_ENDPOINT = "\(URL_SERVER)/service/accept/"
    static let PAYMENT_ENDPOINT = "\(URL_SERVER)/conekta/"
    static let PARTNER_LOCATION_ENDPOINT = "\(URL_SERVER)/partner/location/"
    static let PARTNER_SERVICE_ENDPOINT = "\(URL_SERVER)/partner/service/"
    static let SERVICE_COST_ENDPOINT = "\(URL_SERVER)/service/cost/"
    static let DEVICE_PARTNER_ENDPOINT = "\(URL_SERVER)/device/partner"
    static let DEVICE_USER_ENDPOINT = "\(URL_SERVER)/device/user"
    static let REPORT_USER_ENDPOINT = "\(URL_SERVER)/review/fail/"
    
    class func getSizeForLoadingView(view: UIView) -> CGSize{
        
        return CGSize(width: view.bounds.size.width/4.0, height: view.bounds.size.height/4.0)
        
    }
    
}
