//
//  ServicePartnerViewController.swift
//  Ured
//
//  Created by Isaias Carrera Ventura on 02/01/18.
//  Copyright © 2018 IQBC. All rights reserved.
//

import MapKit
import UIKit
import Kingfisher
import Alamofire
import CoreLocation
import NVActivityIndicatorView
import Cosmos
import SwiftyJSON
import SwiftyButton

class ServicePartnerViewController: UIViewController, NVActivityIndicatorViewable {
    
    @IBOutlet weak var buttonSrvice: FlatButton!
    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var imageViewProfile: UIImageView!
    @IBOutlet weak var hourServiceLabel: UILabel!
    @IBOutlet weak var urgentServiceLabel: UILabel!
    @IBOutlet weak var dateServiceLabel: UILabel!
    @IBOutlet weak var namePartnerLabel: UILabel!
    @IBOutlet weak var descriptionTextView: UITextView!
    @IBOutlet weak var commentView: UIView!
    
    @IBOutlet weak var commentTextView: UITextView!
    @IBOutlet weak var ranking: CosmosView!
    
    var serviceModel: ServiceModel!
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        setupView()
        
        getCommentForService()
     
    }
    
    func getCommentForService(){
        
        let headers : HTTPHeaders = ["Authorization":"JWT \(PartnerController.getPartnerSession()?.value(forKey: "jwt") as! String)"]
        
        Alamofire.request("\(Constants.SERVICE_ENDPOINT)\(String(describing: serviceModel!.idService!))", method: .get, headers: headers)
        .validate()
            .response { (dataResponse) in
                
                guard dataResponse.error == nil else{                    return
                }
                
                guard let data = dataResponse.data else{
                    return
                }
                
                
                
                let json = JSON(data:data)
                
                if self.serviceModel.rejected {
                    
                    guard let rejectedComment = json["rejected_comment"].string else{
                        
                        self.commentView.isHidden = true
                        return
                        
                    }
                    
                    self.ranking.isHidden = true
                    self.commentTextView.text = rejectedComment
                    
                }else{
                    
                    let jsonReview = json["review"]
                    
                    guard let comment = jsonReview["comment"].string, let ranking = jsonReview["ranking"].int else{
                        
                        self.commentView.isHidden = true
                        return
                        
                    }
                    
                    self.commentView.isHidden = false
                    self.ranking.rating = Double(ranking)
                    self.commentTextView.text = comment
                    
                }
                
               
                
        }
        
    }
    
    func setupView(){
        
        CustomBar.setNavBar((self.navigationController?.navigationBar)!, navItem: navigationItem,nav: self)
        
        self.namePartnerLabel.text = (serviceModel.partnerModel?.value(forKey: "name") as! String) + " " + (serviceModel.partnerModel?.value(forKey: "lastname") as! String)
        self.dateServiceLabel.text = serviceModel.date!
        self.hourServiceLabel.text = serviceModel.hour!
        self.urgentServiceLabel.textColor = serviceModel.express ? ColorUtils.getColorUredApp(1.0) : ColorUtils.getGrayColorApp(1.0)
        self.descriptionTextView.text = serviceModel.description!
        
        if let url = URL(string: serviceModel.userModel?.value(forKey: "imageUrl") as! String){
            
            KingfisherManager.shared.retrieveImage(with: url, options: [.transition(.fade(1))], progressBlock: nil, completionHandler: { image, error, cacheType, imageURL in
                
                if error == nil {
                    
                    self.imageViewProfile.image = image
                    
                }
                
            })
            
        }
        
        if serviceModel.rejected || serviceModel.paid || serviceModel.quoted || serviceModel.accepted {
            self.buttonSrvice.isHidden = true
        }
        
        let location = CLLocationCoordinate2D(latitude: serviceModel.location!.latitude, longitude: serviceModel.location!.longitude)
        ImageUtils.circularImage(imageViewProfile, color: ColorUtils.getColorUredApp(1.0).cgColor)

        let pointAnnotation = MKPointAnnotation()
        pointAnnotation.coordinate = CLLocationCoordinate2D(latitude: location.latitude, longitude: location.longitude)
        pointAnnotation.title = serviceModel.location!.address!
        mapView.addAnnotation(pointAnnotation)
        
        let center = CLLocationCoordinate2D(latitude: location.latitude, longitude: location.longitude)
        let region = MKCoordinateRegion(center: center, span: MKCoordinateSpan(latitudeDelta: 0.01, longitudeDelta: 0.01))
        
        mapView.setRegion(region, animated: true)
        
        
    }


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
   
    @IBAction func acceptService(_ sender: Any) {
        
         if !serviceModel.quoted || !serviceModel.paid{
            
            //1. Create the alert controller.
            let alert = UIAlertController(title: "Enviar cotización", message: "Agrega el precio del servicio.", preferredStyle: .alert)
            
            
            //2. Add the text field. You can configure it however you need.
            alert.addTextField { (textField) in
                
                
                textField.keyboardType = .numberPad
                
            }
            
            // 3. Grab the value from the text field, and print it when the user clicks OK.
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { [weak alert] (_) in
                
                let textField = alert?.textFields![0] // Force unwrapping because we know it exists.
                if let amount = textField?.text {
                    
                    let params : Parameters = ["cost":amount]
                    let headers : HTTPHeaders = ["Authorization" : "JWT \(PartnerController.getPartnerSession()?.value(forKey: "jwt") as! String)"]
                    
                    
                    self.startAnimating(Constants.getSizeForLoadingView(view: self.view), type: NVActivityIndicatorType.ballBeat, color: ColorUtils.getColorUredApp(1.0), padding: NVActivityIndicatorView.DEFAULT_PADDING)
                    
                    Alamofire.request(Constants.SERVICE_COST_ENDPOINT + self.serviceModel.idService!, method: .put, parameters: params, headers: headers).validate()
                    .response(completionHandler: { (dataResponse) in
                        
                        self.stopAnimating()
                        
                        if dataResponse.error == nil {
                            
                            if dataResponse.response?.statusCode == 200 {
                                
                                _ = SweetAlert().showAlert("Cotización enviada", subTitle: "Enviaste la cotización de forma éxitosa.", style: .success, buttonTitle: "Ok", action: { (flag) in
                                    
                                    self.navigationController?.popToRootViewController(animated: true)
                                    
                                })
                                
                            }else{
                                
                                _ = SweetAlert().showAlert("Ops!", subTitle: "Algo salió mal.", style: AlertStyle.error, buttonTitle:"Ok", buttonColor:ColorUtils.getGreenColorApp(1.0))
                                
                            }
                            
                        }else{
                            
                            
                            _ = SweetAlert().showAlert("Ops!", subTitle: "Algo salió mal.", style: AlertStyle.error, buttonTitle:"Ok", buttonColor:ColorUtils.getGreenColorApp(1.0))
                            
                        }
                        
                    })
                    
                }else{
                    
                    
                    _ = SweetAlert().showAlert("Ops!", subTitle: "Ingresa el precio del servicio", style: AlertStyle.error, buttonTitle:"Ok", buttonColor:ColorUtils.getGreenColorApp(1.0))
                    
                }
                
                
            }))
            
            // 4. Present the alert.
            self.present(alert, animated: true, completion: nil)

            
        }else{
            
            _ = SweetAlert().showAlert("Ops!", subTitle: "Ya cotizaste el servicio previamente.", style: AlertStyle.error, buttonTitle:"Ok", buttonColor:ColorUtils.getGreenColorApp(1.0))
            
            
        }
        
    }
}
